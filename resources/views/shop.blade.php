@extends('User.navbar')

@section('title')
    <title>Shop</title>
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/bootstrap4/bootstrap.min.css')}}">
<link href="{{asset('user/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('user/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/plugins/jquery-ui-1.12.1.custom/jquery-ui.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/shop_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/shop_responsive.css')}}">
<link rel="stylesheet" href="{{asset('admin/by_myself/custom.css')}}">
@endsection

@section('content')
    <div class="home">
        <div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{asset('user/img/shop_background.jpg')}}"></div>
        <div class="home_overlay"></div>
        <div class="home_content d-flex flex-column align-items-center justify-content-center">
            <h2 class="home_title">Smartphones & Tablets</h2>
        </div>
    </div>

    <div class="shop">
		<div class="container">
			<div class="row">
				<div class="col-lg-3">

					<!-- Shop Sidebar -->
					<div class="shop_sidebar">
						<div class="sidebar_section">
							<div class="sidebar_title">Categories</div>
							<ul class="sidebar_categories">
                                @foreach ($categories as $kategori)
                                <li><a href="{{route('filter.categories',['nama_kategori'=>$kategori->nama_kategori])}}">{{$kategori->nama_kategori}}</a></li>
                                @endforeach
							</ul>
						</div>
						{{-- <div class="sidebar_section filter_by_section">
							<div class="sidebar_title">Filter By</div>
							<div class="sidebar_subtitle">Price</div>
							<div class="filter_price">
								<div id="slider-range" class="slider_range"></div>
								<p>Range: </p>
								<p><input type="text" id="amount" class="amount" name="find_price" readonly style="border:0; font-weight:bold;"></p>
							</div>
						</div> --}}
						{{-- <div class="sidebar_section">
							<div class="sidebar_subtitle color_subtitle">Color</div>
							<ul class="colors_list">
								<li class="color"><a href="#" style="background: #b19c83;"></a></li>
								<li class="color"><a href="#" style="background: #000000;"></a></li>
								<li class="color"><a href="#" style="background: #999999;"></a></li>
								<li class="color"><a href="#" style="background: #0e8ce4;"></a></li>
								<li class="color"><a href="#" style="background: #df3b3b;"></a></li>
								<li class="color"><a href="#" style="background: #ffffff; border: solid 1px #e1e1e1;"></a></li>
							</ul>
						</div> --}}
						{{-- <div class="sidebar_section">
							<div class="sidebar_subtitle brands_subtitle">Brands</div>
							<ul class="brands_list">
								<li class="brand"><a href="#">Apple</a></li>
								<li class="brand"><a href="#">Beoplay</a></li>
								<li class="brand"><a href="#">Google</a></li>
								<li class="brand"><a href="#">Meizu</a></li>
								<li class="brand"><a href="#">OnePlus</a></li>
								<li class="brand"><a href="#">Samsung</a></li>
								<li class="brand"><a href="#">Sony</a></li>
								<li class="brand"><a href="#">Xiaomi</a></li>
							</ul>
						</div> --}}
					</div>

				</div>

				<div class="col-lg-9">

					<!-- Shop Content -->

					<div class="shop_content">
						<div class="shop_bar clearfix">
							<div class="shop_product_count"><span>{{$products->total()}} </span>  Products found</div>
							<div class="shop_sorting">
								<span>Sort by:</span>
								<ul>
									<li>
										<span class="sorting_text">highest rated<i class="fas fa-chevron-down"></span></i>
										<ul>
											<li class="shop_sorting_button" data-isotope-option='{ "sortBy": "original-order" }'>highest rated</li>
											<li class="shop_sorting_button" data-isotope-option='{ "sortBy": "name" }'>name</li>
											<li class="shop_sorting_button" data-isotope-option='{ "sortBy": "price" }'>harga</li>
										</ul>
									</li>
								</ul>
							</div>
						</div>

						<div class="product_grid">
							<div class="product_grid_border"></div>

                            <!-- Product Item -->
                            @foreach ($products as $product)
							<div class="product_item is_new">
                                <a class="wawawi" href="{{route('page.singleProduct',['id_product'=>$product->id_product])}}" tabindex="0" class="">
                                    <div class="product_border"></div>
								    <div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{asset('css/img/'. $product->gambar_product)}}" alt=""></div>
                                </a>
                                <div class="product_content">
									<div class="product_price">{{App\product::konversi($product->harga)}}</div>
									<div class="product_name"><div><a href="{{route('page.singleProduct',['id_product'=>$product->id_product])}}" tabindex="0" class="">{!! \Illuminate\Support\Str::words($product->nama_barang, 3, '...')!!}</a></div></div>
                                </div>
								{{-- <div class="product_fav"><i class="fas fa-heart"></i></div> --}}
								<ul class="product_marks">
                                    {{-- <li class="product_mark product_discount">-25%</li> --}}
									{{-- <li class="product_mark product_new">new</li> --}}
								</ul>
                            </div>
                            @endforeach
					    </div>

					</div>

						<!-- Shop Page Navigation -->

					<div class="shop_page_nav d-flex flex-row">
						{{-- <div class="page_prev d-flex flex-column align-items-center justify-content-center"><i class="fas fa-chevron-left"></i></div> --}}
						{{-- <ul class="page_nav d-flex flex-row"> --}}
                            {{$products->links()}}
						{{-- </ul> --}}
						{{-- <div class="page_next d-flex flex-column align-items-center justify-content-center"><i class="fas fa-chevron-right"></i></div> --}}
					</div>

				</div>
			</div>
		</div>
	</div>

    <div class="viewed">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="viewed_title_container">
						<h3 class="viewed_title">Recently Viewed</h3>
						<div class="viewed_nav_container">
							<div class="viewed_nav viewed_prev"><i class="fas fa-chevron-left"></i></div>
							<div class="viewed_nav viewed_next"><i class="fas fa-chevron-right"></i></div>
						</div>
					</div>

					<div class="viewed_slider_container">

						<!-- Recently Viewed Slider -->

						<div class="owl-carousel owl-theme viewed_slider">

                            <!-- Recently Viewed Item -->
                            @foreach ($recent_products as $recent)
							<div class="owl-item">
								<div class="viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
									<div class="viewed_image"><img src="{{asset('css/img/'. $recent->gambar_product)}}" alt=""></div>
									<div class="viewed_content text-center">
										<div class="viewed_price">{{App\product::konversi($recent->harga)}}<span></span></div>
										<div class="viewed_name"><a href="{{route('page.singleProduct',['id_product'=>$recent->id_product])}}">{!! \Illuminate\Support\Str::words($recent->nama_barang, 3, '...')!!}</a></div>
									</div>
									<ul class="item_marks">
										<li class="item_mark item_discount">0%</li>
										<li class="item_mark item_new">new</li>
									</ul>
								</div>
                            </div>
                            @endforeach
						</div>

					</div>
				</div>
			</div>
		</div>
    </div>
    {{-- @section('script')
    <script src="{{asset('admin/js/shop_custom.js')}}"></script>
    @endsection --}}


@endsection
@section('script')
    <script src="{{asset('admin/js/shop_custom.js')}}"></script>
@endsection
