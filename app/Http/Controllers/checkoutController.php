<?php

namespace App\Http\Controllers;

use App\City;
use App\Courier;
use App\order;
use App\order_product;
use App\product;
use App\Province;
use App\users;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Kavist\RajaOngkir\Facades\RajaOngkir;


class checkoutController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        if ($request->session()->has('cart')) {
            # code...
            // $test[] = $request->cookie('id-product');
            $categories = DB::table('kategori_barang')->get();
            $couriers = Courier::pluck('title', 'code');
            $provinces = Province::pluck('province_name', 'province_id');
            $asal_kota = City::where('city_name', 'Bandung');
        }

        else {
            return redirect()->route('page.cart');
        }

        $data = array(
            'categories' => $categories,
        );

        return view('checkout', $data ,compact('provinces', 'couriers', 'asal_kota'));
    }

    public function getCities($id)
    {
        $city = City::where('province_id', $id)->pluck('city_name','city_id');
        return json_encode($city);
    }

    public function submit(Request $request)
    {
        $categories = DB::table('kategori_barang')->get();

        $client = new Client();

        $response = $client->request('POST','https://api.rajaongkir.com/starter/cost',
        [
            'body' =>'origin=22&destination='.$request->city_destination.'&weight='.$request->weight.'&courier='.$request->courier.'',
            'headers' =>[
                'key' => '85acc12f7d4bcc6fa042f4a72bc41f8a',
                'content-type' => 'application/x-www-form-urlencoded',
            ]
        ]);

        $json = $response->getBody()->getContents();

        $array_result = json_decode($json, true);

        $origin = $array_result["rajaongkir"]["origin_details"]["city_name"];
        $postal_origin = $array_result["rajaongkir"]["origin_details"]["postal_code"];
        $destination = $array_result["rajaongkir"]["destination_details"]["city_name"];
        $postal_destination = $array_result["rajaongkir"]["destination_details"]["postal_code"];
        $ongkir = $array_result["rajaongkir"]["results"];


        $data = array(
            // 'couriers' => $couriers,
            'categories' => $categories,
            'origin' => $origin,
            'destination' => $destination,
            'array_result' => $array_result,
            'ongkir' => $ongkir,
            'postal_origin' => $postal_origin,
            'postal_destination' =>$postal_destination,
        );

        return view('test', $data);

    }

    public function checkOut(Request $request){
        // dd($request);
        $order = order::create(array(
            'id_user' => Auth::user()->id,
            'tujuan_provinsi' => Request('province_destination'),
            'tujuan_kota' => Input::get('city_destination'),
            'kurir' => Input::get('courier'),
            'berat' => $request->input('weight'),
        ));

        foreach ($request->input('product') as $value) {
            $order_product = new order_product();
            // $order_barang = order_product::create(array(
            //     'id_order' => $order->id_order,
            //     'id_product' => Input::get('id_product'),
            //     'quantity' => Input::get('quantity'),
            // ));
            $order_product->id_order = $order->id_order;
            $order_product->id_product = $value['id_product'];
            $order_product->quantity_order = $value['quantity'];
            $order_product->save();
        }

        // $categories = DB::table('kategori_barang')->get();
        // $user = users::find(Auth::user()->id);

        $ongkir = $request->input('harga-ongkir');
        $id_order = $order->id_order;

        $total_order = $request->input('total-bayar');

        // $harga_transaksi = session()->set('harga_transaksi');

        $data = array(
            'ongkir' => $ongkir,
            'total_order' => $total_order,
            'id_order' => $id_order,
            // 'categories' => $categories,
            // 'user' => $user,
        );


        session()->put('tests', $data);


        $order->save();

        return redirect()->route('shipping');
        // return view('shipping', $data);
    }
}

