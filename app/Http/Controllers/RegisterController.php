<?php

namespace App\Http\Controllers;

use App\users;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function create(){
        return view('register');
    }

    public function Doregister(Request $request){

        $this->validate($request,[
            'name_user' => 'required',
            'username' => 'required|min:3',
            'email' => 'required|email',
            'password' => 'required|min:5|confirmed',

        ]);

        $user = new \App\users;

        $user->name_user = $request->name_user;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        // $user = users::create(request(['name_user','username','email','password']));
        return redirect('login');
    }
}

