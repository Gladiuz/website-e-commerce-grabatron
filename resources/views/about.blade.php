@extends('User.navbar')

@section('title')
    <title>About Us</title>
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('user/styles/bootstrap4/bootstrap.min.css')}}">
    <link href="{{asset('user/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('user/styles/contact_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/styles/contact_responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/by_myself/custom.css')}}">
@endsection

@section('content')
    <div class="home">
		<div class="home_background parallax-window" data-parallax="scroll" data-image-src="{{asset('user/img/blog_single_background.jpg')}}" data-speed="0.8"></div>
	</div>

   <div class="single_post m-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
                    <div class="single_post_title about-title">About Us</div>
                    <hr>
					<div class="single_post_text">
                        @foreach ($about as $bout)
						<p class="about-desc">{{$bout->deskripsi}}</p>
                        @endforeach

						{{-- <div class="single_post_quote text-center">
							<div class="quote_image"><img src="images/quote.png" alt=""></div>
							<div class="quote_text">Quisque sagittis non ex eget vestibulum. Sed nec ultrices dui. Cras et sagittis erat. Maecenas pulvinar, turpis in dictum tincidunt, dolor nibh lacinia lacus.</div>
							<div class="quote_name">Liam Neeson</div>
						</div> --}}

						{{-- <p>Praesent ac magna sed massa euismod congue vitae vitae risus. Nulla lorem augue, mollis non est et, eleifend elementum ante. Nunc id pharetra magna.  Praesent vel orci ornare, blandit mi sed, aliquet nisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. </p> --}}
					</div>
				</div>
			</div>
		</div>
    </div>
    <div class="contact_info">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="contact_info_container d-flex flex-lg-row flex-column justify-content-between align-items-between">

						<!-- Contact Item -->
						<div class="contact_info_item d-flex flex-row align-items-center justify-content-start">
							<div class="contact_info_image"><img src="{{asset('user/img/contact_1.png')}}" alt=""></div>
							<div class="contact_info_content">
								<div class="contact_info_title">Phone</div>
								<div class="contact_info_text">+38 068 005 3570</div>
							</div>
						</div>

						<!-- Contact Item -->
						<div class="contact_info_item d-flex flex-row align-items-center justify-content-start">
							<div class="contact_info_image"><img src="{{asset('user/img/contact_2.png')}}" alt=""></div>
							<div class="contact_info_content">
								<div class="contact_info_title">Email</div>
								<div class="contact_info_text">fastsales@gmail.com</div>
							</div>
						</div>

						<!-- Contact Item -->
						<div class="contact_info_item d-flex flex-row align-items-center justify-content-start">
							<div class="contact_info_image"><img src="{{asset('user/img/contact_3.png')}}" alt=""></div>
							<div class="contact_info_content">
								<div class="contact_info_title">Address</div>
								<div class="contact_info_text">10 Suffolk at Soho, London, UK</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
    </div>
    <div class="contact_form">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="contact_form_container">
						<div class="contact_form_title">Get in Touch</div>

                        <form action="{{route('inbox.added')}}" method="POST" enctype="multipart/form-data" id="contact_form">
                            {{ csrf_field() }}
                            @if (Auth::check())
                            <div class="contact_form_inputs">
                                <input type="text" id="contact_form_name" class="contact_form_name input_field mr-2" value="{{Auth::user()->name_user}}" name="name_user" placeholder="Your name" required="required"  data-error="Name is required." >
								<input type="text" id="contact_form_email" class="contact_form_email input_field mr-2" value="{{Auth::user()->email}}" name="email" placeholder="Your email" required="required" data-error="Email is required.">
                            </div>
                            @endif
                            @if (!Auth::check())
                            <div class="contact_form_inputs">
								<input type="text" id="contact_form_name" class="contact_form_name input_field mr-2" placeholder="Your name" name="name_user" required="required" data-error="Name is required.">
                                <input type="text" id="contact_form_email" class="contact_form_email input_field mr-2" placeholder="Your email" name="email" required="required" data-error="Email is required.">
                            </div>
                            @endif
                            <div class="contact_form_inputs">
                                <input type="text" id="contact_form_email" class="input-color contact_form_email input_field input-subject" name="subject" placeholder="Input your Subject" required="required" data-error="Subject is required">
                            </div>
								{{-- <input type="text" id="contact_form_phone" class="contact_form_phone input_field" placeholder="Your phone number"> --}}
							<div class="contact_form_text">
								<textarea id="contact_form_message" class="text_field contact_form_message" rows="4" placeholder="Message" name="description" required="required" data-error="Please, write us a message."></textarea>
							</div>
							<div class="contact_form_button">
								<button type="submit" class="button contact_submit_button">Send Message</button>
							</div>
						</form>

					</div>
				</div>
			</div>
		</div>
		<div class="panel"></div>
	</div>

@endsection

@section('script')
    <script src="{{asset('admin/js/contact_custom.js')}}"></script>
@endsection
