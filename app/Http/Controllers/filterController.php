<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class filterController extends Controller
{
    //
    public function filter($id)
    {
        $categories = DB::table('kategori_barang')->get();
        $recent_products = DB::table('product')->orderBy('created_at','DESC')->paginate(6);
        $products = DB::table('product')
        ->leftjoin('kategori_barang','product.id_kategori','=','kategori_barang.id_kategori')
        ->where('kategori_barang.nama_kategori','=', $id)
        ->paginate(15);

        $data = array(
            'products' => $products,
            'categories' => $categories,
            'recent_products' => $recent_products,
        );

        return view('shop', $data);
    }
}
