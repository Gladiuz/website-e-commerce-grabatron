@extends('User.navbar')
@section('title')
    <title>Cart</title>
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/bootstrap4/bootstrap.min.css')}}">
<link href="{{asset('user/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('user/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/cart_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/cart_responsive.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/smart_cart.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('admin/by_myself/custom1.css')}}">
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> --}}

@endsection

@section('content')

<div class="cart_section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 ">
                @if (session('cart'))
                <div class="cart_container">
                    <div class="cart_title">Shopping Cart</div>
                    <div class="cart_items">
                        <ul class="cart_list">
                            <?php $total = 0 ?>
                            @foreach ((array) session('cart') as $id => $details)
                            <?php $total += $details['harga'] * $details['quantity'] ?>
                            <tr>
                            <span id="sss"></span>
                            <li class="cart_item clearfix test">
                                <div class="cart_item_image"><img src="{{asset('css/img/'. $details['gambar_product'])}}" alt=""></div>
                                <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
                                    <div class="cart_item_name cart_info_col">
                                        <div class="cart_item_title">Name</div>
                                        <div class="cart_item_text">{!! \Illuminate\Support\Str::words($details['nama_barang'], 3, '...')!!}</div>
                                    </div>
                                    <div class="cart_item_color cart_info_col">
                                        <div class="cart_item_title">Category</div>
                                        <div class="cart_item_text"></span>{!! \Illuminate\Support\Str::words($details['id_kategori'], 3, '...')!!}</div>
                                    </div>
                                    <div class="cart_item_quantity cart_info_col">
                                        <div class="cart_item_title">Quantity</div>
                                        <div class="cart_item_text"><input type="number" name="quantity" min="1" max="50s" value="{{ $details['quantity'] }}"class="form-control update-cart quantity" data-id="{{ $id }}" /></div>
                                    </div>
                                    <div class="cart_item_price cart_info_col">
                                        <div class="cart_item_title">Price</div>
                                        <div class="cart_item_text">{{App\product::konversi($details['harga'])}}</div>
                                    </div>
                                    <div class="cart_item_total cart_info_col">
                                        <div class="cart_item_title">Total</div>
                                        <div class="cart_item_text product-subtotal">{{App\product::konversi($details['harga'] * $details['quantity'])}}</div>
                                    </div>
                                    <div class="cart_item_total cart_info_col">
                                        {{-- <button class="btn btn-transparent btn-sm update-cart" data-id="{{ $id }}"><i class="fa fa-refresh"></i></button> --}}
                                        <button class="btn btn-danger btn-sm remove-cart" data-id="{{ $id }}"><i class="fa fa-trash-o"></i></button>
                                        {{-- <i class="fa fa-circle-o-notch fa-spin btn-loading" style="font-size:24px; display: none"></i> --}}
                                    </div>

                                </div>
                            </li>
                            </tr>
                            @endforeach
                        </ul>
                    </div>

                    <!-- Order Total -->
                    <div class="order_total">
                        <div class="order_total_content text-md-right">
                            <div class="order_total_title">Order Total:</div>
                            <div class="order_total_amount"><span class="cart-total">{{App\product::konversi($total)}}</span></div>
                        </div>
                    </div>

                    <div class="cart_buttons">
                        <a href="{{route('page.shop')}}" type="button" class="button cart_button_checkout float_left">Continue Shopping</a>
                        <a href="{{route('delete.all')}}" type="button" class="button cart_button_clear" onclick="return confirm('Are you sure you want delete all product in cart ?')" >
                            Clear Cart</a>
                        <a href="{{route('checkout')}}" type="button" class="button cart_button_checkout">Check Out</a>
                    </div>
                </div>
                @endif
                @if (!session('cart'))
                <div class="btn-center">
                    <img src="{{asset('user/img/cart-cloud.jpg')}}" class="rounded mx-auto d-block cart-image" alt="">
                    <h4 class="text-center">Tidak ada barang di keranjang belanja kamu</h4>
                    <p class="text-center">Mulailah belanja dan nikmati kemudahannya</p>
                    <a href="{{route('page.shop')}}" type="button" class="button cart_button_checkout">Pergi ke Shop</a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

<!-- Newsletter -->

<div class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
                    <div class="newsletter_title_container">
                        <div class="newsletter_icon"><img src="{{asset('user/img/send.png')}}" alt=""></div>
                        <div class="newsletter_title">Sign up for Newsletter</div>
                        <div class="newsletter_text"><p>...and receive %20 coupon for first shopping.</p></div>
                    </div>
                    <div class="newsletter_content clearfix">
                        <form action="#" class="newsletter_form">
                            <input type="email" class="newsletter_input" required="required" placeholder="Enter your email address">
                            <button class="newsletter_button">Subscribe</button>
                        </form>
                        <div class="newsletter_unsubscribe_link"><a href="#">unsubscribe</a></div>
                    </div>
                    a
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
{{-- <script src="//code.jquery.com/jquery.min.js"></script> --}}
{{-- <script src="{{asset('user/plugins/smartCart.min.js')}}"></script> --}}
{{-- <script src="{{asset('user/plugins/tst.js')}}"></script> --}}
{{-- <script src="{{asset('user/plugins/smart-cart-js/jquery.mycart.js')}}"></script> --}}
{{-- <script src="{{asset('admin/js/cart_custom.js')}}"></script> --}}
{{-- <script src="{{asset('admin/by_myself/test.js')}}"></script> --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {



        $(".update-cart").click(function (e) {
            e.preventDefault();

            var ele = $(this);

            var parent_row = ele.parents("li");

            var quantity = parent_row.find(".quantity").val();

            var product_subtotal = parent_row.find(".product-subtotal");

            var cart_total = $(".cart-total");

            // var loading = $(".btn-loading");

            // loading.show();


            $.ajax({
                url: '{{ url('update-cart') }}',
                method: "post",
                data: {_token: '{{ csrf_token() }}', id_product: ele.attr("data-id"), quantity: quantity},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "json",
                success: function (response) {
                    // window.location.reload();

                    // loading.hide();
                    // alert("test");

                    // $("#status").html('<div class="alert alert-success">'+response.msg+'</div>');

                    // $("#sss").html(response.data);

                    product_subtotal.html(response.subTotal);

                    cart_total.html(response.total);
                },

                error: function (xhr ,eror, status){
                    // alert("kesalahan pada ajax")
                    var err = eval("(" + xhr.responseText + ")");
                    alert(err.Message);
                }
            });
        });

        $(".remove-cart").click(function (e) {
                e.preventDefault();

                var ele = $(this);

                // var parent_row = $(".test");

                // var cart_total = $(".cart-total");

                // parent_row.fadeOut('slow');


                if(confirm("Are you sure you want to delete this product ?")) {
                    $.ajax({
                        url: '{{ url('remove-cart') }}',
                        method: "delete",
                        data: {_token: '{{ csrf_token() }}', id_product: ele.attr("data-id")},
                        dataType: "json",
                        success: function (response) {
                            window.location.reload();
                            // $(".test").fadeOut('slow');

                            // $("span#status").html('<div class="alert alert-success">'+response.msg+'</div>');

                            // $("#header-bar").html(response.data);

                            // cart_total.text(response.total);
                        },

                        error: function (xhr ,eror, status){
                            var err = eval("(" + xhr.responseText + ")");
                            alert(err.Message);
                }

                    });
                }
            });
    });
</script>
@endsection


