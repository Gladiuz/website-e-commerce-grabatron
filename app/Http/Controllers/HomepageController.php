<?php

namespace App\Http\Controllers;

use App\berita;
use App\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomepageController extends Controller
{
    //

    public function logoutUser(){
        Auth::logout();

        session()->forget('tests');
        return redirect()->route('home');
    }

    public function searchProduct(Request $request)
    {
        if ($request->has('search_product') || Auth::check()) {
            $products = product::where('nama_barang','LIKE','%'.$request->search_product.'%')
            ->paginate(15);
            $categories = DB::table('kategori_barang')->get();
            $recent_products = DB::table('product')->orderBy('created_at','DESC')->paginate(6);
        }
        else {
            return route('page.shop');
        }

        $data = array(
            'products' => $products,
            'categories' => $categories,
            'recent_products' => $recent_products,
        );

        return view('shop', $data);
    }

    public function getIndex()
    {
        $products = DB::table('product')->orderBy('created_at','DESC')->paginate(10);
        $banners = DB::table('banner')->get();
        $categories = DB::table('kategori_barang')->get();



        $data = array(
            'banners' => $banners,
            'products' => $products,
            'categories' => $categories,
        );


        return view('welcome', $data);
        // $value = $request->cookie('mama');
        // echo $value;
    }

    public function showShop(Request $request)
    {
        if ($request->has('find_price')) {
            $products = product::where('harga','LIKE','%'.$request->find_price.'%')->paginate(15);
        }

        // $banners = DB::table('banner')->get();
        $products = DB::table('product')->inRandomOrder()->paginate(15);
        $categories = DB::table('kategori_barang')->get();

        $recent_products = DB::table('product')->orderBy('created_at','DESC')->paginate(6);

        $data = array(
            // 'banners' => $banners,
            'products' => $products,
            'categories' => $categories,
            'recent_products' => $recent_products,

        );

        return view('shop', $data);
    }

    public function showBlog()
    {
        $news = DB::table('berita')->paginate(9);
        $banners = DB::table('banner')->get();
        $products = DB::table('product')->paginate(15);
        $categories = DB::table('kategori_barang')->get();
        $recent_products = DB::table('product')->orderBy('created_at','DESC')->paginate(6);


        $data = array(
            'news' => $news,
            'banners' => $banners,
            'products' => $products,
            'categories' => $categories,
            'recent_products' => $recent_products,

        );
        return view('blog', $data);
    }

    public function showSingleBlog($id)
    {
        $berita = berita::find($id);
        $banners = DB::table('banner')->get();
        $news = DB::table('berita')->orderBy('created_at','ASC')->paginate(3);
        // $products = DB::table('product')->paginate(15);
        $categories = DB::table('kategori_barang')->get();
        // $recent_products = DB::table('product')->orderBy('created_at','DESC')->paginate(6);


        $data = array(
            'berita' => $berita,
            'banners' => $banners,
            'news' => $news,
            'categories' => $categories,
            // 'recent_products' => $recent_products,
        );
        return view('blog_full', $data);
    }

    public function showSingleProduct($id)
    {

        $product = product::where('product.id_product', '=' , $id)->leftjoin('kategori_barang','product.id_kategori','=','kategori_barang.id_kategori')->first();
        $categories = DB::table('kategori_barang')->get();

        $recent_products = DB::table('product')->orderBy('created_at','DESC')->paginate(6);


        $data = array(
            'product' => $product,
            'categories' => $categories,
            'recent_products' => $recent_products,
        );

        return view('shop_full', $data);
    }

    public function showAboutUs()
    {
        $categories = DB::table('kategori_barang')->get();
        $about = DB::table('about')->get();

        $data = array(
            'categories' => $categories,
            'about' => $about,
        );

        return view('about',$data);
    }

}
