<?php

namespace App\Http\Controllers;

use App\banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;

class bannerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application Banner.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->has('searching')) {
            $banners = banner::where('title','LIKE','%'.$request->searching.'%')->paginate('10');
            $inboxes = DB::table('inbox')->paginate();
        }
        else {
            $banners = DB::table('banner')->paginate('10');
            $inboxes = DB::table('inbox')->paginate();
        }

        $data = array(
            'banners'=> $banners,
            'inboxes' => $inboxes,
        );

        return view('admin/banner',$data);
    }

    public function indexs()
    {
        $inboxes = DB::table('inbox')->paginate();
        return view('/admin/banner_create',['inboxes'=>$inboxes]);
    }

    public function logout(){
        Auth::logout();

        return redirect()->route('login');
    }

    public function createBanner(Request $request){

        $this->validate($request,[
            'title' => 'required|min:5',
            'description' => 'required',
            'thumbnail' => 'required',
        ]);

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailname = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->save('css/img/'.$thumbnailname);
        }
        $banners = banner::create(array(
            'title' => Input::get('title'),
            'description' => Input::get('description'),
            'thumbnail' => $thumbnailname,
        ));
        $banners->save();
        return redirect()->route('banner')->with('success','Banner has been successfully added!');
    }

    public function edit($id)
    {
        $banner = banner::find($id);
        $inboxes = DB::table('inbox')->paginate();

        $data = array(
            'banner'=> $banner,
            'inboxes' => $inboxes,
        );

        return view ('admin.bannerEdit',$data);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'title' => 'required|min:5',
            'description' => 'required',
        ]);

        // dd( $request->file('thumbnail')->getClientOriginalName());
        $banners = banner::find($id);
        $banners->title = $request->title;
        $banners->description = $request->description;

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $extension = $thumbnail->getClientOriginalExtension();
            $thumbnailname = time() . '.' . $extension;
            $thumbnail->move('css/img/',$thumbnailname);
            $banners->thumbnail = $thumbnailname;
        }

        $banners->save();

        return redirect('banner')->with('success','Banner has been updated successfully');
    }
    public function delete($id)
    {
        $banners = banner::find($id);
        $banners->delete($banners);
        return redirect('banner')->with('success','Banner has been deleted successfully');
    }
}
