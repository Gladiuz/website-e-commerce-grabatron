@extends('layouts/navbar')
@section('title')
    <title>Banner</title>
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Banner</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Banner</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Title</h3>

            <div class="card-tools">
                <a href="{{route('create')}}">
                    <button type="button" class="btn btn-block btn-primary btn-sm">Create Banner</button>
                </a>
            </div>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Thumbnail</th>
                  <th>Title</th>
                  <th>Deskripsi</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($banners as $banner)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td class="col-5">
                            <img src="{{asset('css/img/'. $banner->thumbnail)}}" class="img-fluid img-thumbnail img-responsive" style="width: 50%" alt="Sheep">
                        </td>
                        <td class="col-2">{{$banner->title}}</td>
                        <td>{!! \Illuminate\Support\Str::words($banner->description, 7, '...')!!}</td>
                        <td class="col-2">
                            <a href="{{route('banner.edit',['id' => $banner->id])}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-edit"></i> Edit</a>
                            {{-- <hr> --}}
                            <a href="{{route('banner.delete',['id'=> $banner->id])}}" class="btn btn-danger btn-sm" onclick="return confirm(
                                'Are you sure you want to delete this banner ?')">
                                <i class="fa fa-times-circle"></i> Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Thumbnail</th>
                  <th>Title</th>
                  <th>Deskripsi</th>
                  <th>Action</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection
