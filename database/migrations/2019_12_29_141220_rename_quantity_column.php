<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameQuantityColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('product', function(Blueprint $table) {
            $table->renameColumn('quantity','quantity_product');
        });

        Schema::table('order_product', function(Blueprint $table) {
            $table->renameColumn('quantity','quantity_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product', function(Blueprint $table) {
            $table->renameColumn('quantity','quantity_product');
        });

        Schema::table('order_product', function(Blueprint $table) {
            $table->renameColumn('quantity','quantity_order');
        });
    }
}
