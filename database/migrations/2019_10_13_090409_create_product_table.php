<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_barang');
            $table->string('deskripsi');
            $table->integer('harga', 10);
            $table->unsignedInteger('id_kategori');
            $table->string('gambar_product');
            $table->timestamps();
        });

        Schema::table('product', function (Blueprint $table){
            $table->foreign('id_kategori')->references('id')->on('kategori_barang')->onDelete('cascade')->onUpdate('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
