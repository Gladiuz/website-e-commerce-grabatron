@extends('layouts.navbar')
@section('title')
    <title>Create Produk</title>
@endsection

@section('content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Banner Create</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Banner</li>
              <li class="breadcrumb-item active">Banner Create</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Banner Create</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{ route('produk.make') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputTitle">Nama Barang</label>
                      <input type="text" class="form-control" name="nama_barang" id="exampleInputTitle" placeholder="Masukan Nama Barang">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Deskripsi Barang</label>
                      <textarea class="form-control" name="deskripsi" rows="3" placeholder="Enter ..."></textarea>
                    </div>
                    <div class="form-group">
                        <label class="" for="select1">Status</label>
                        {{-- <div class="col-md-9"> --}}
                          <select class="form-control" name="id_kategori">
                            <option>Please select</option>
                            @foreach ($categories as $category)
                                <option value="{{$category->id_kategori}}">{{$category->nama_kategori}}</option>
                            @endforeach
                          </select>
                        {{-- </div> --}}
                      </div>
                    <div class="form-group">
                        <label for="exampleInputTitle">Quantity</label>
                        <input type="text" class="form-control" name="quantity_product" id="exampleInputTitle" placeholder="Masukan Quantity">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputTitle">Harga</label>
                        <input type="text" class="form-control" name="harga" id="exampleInputTitle" placeholder="Masukan Harga">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File input Thumbnail</label>
                      <div class="input-group">
                        <div class="custom-file">
                          <input type="file" class="custom-file-input" name="gambar_product" id="exampleInputFile">
                          <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                          <span class="input-group-text" id="">Upload</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
