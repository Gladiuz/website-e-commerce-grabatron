<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    //
    protected $table = 'product';
    protected $primaryKey = 'id_product';

    protected $fillable = [
        'nama_barang','deskripsi','harga','id_kategori','quantity_product','gambar_product'
    ];

    public function kategori()
    {
        return $this->belongsTo('App\kategori','foreign_key','other_key');
    }





    public static function konversi($angka)
    {
        return 'Rp. '.strrev(implode('.',str_split(strrev(strval($angka)),3)));
    }

    public function order_product()
    {
        return $this->hasMany('App\order_product');
    }
}
