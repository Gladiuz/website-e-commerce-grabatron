<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    //tampilan buat di dasboard
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = DB::table('product')
        ->orderBy('created_at','DESC')
        ->paginate(5);
        $users = DB::table('users')->paginate(8);
        $kategori_barang = DB::table('kategori_barang')->get();
        $inboxes = DB::table('inbox')->paginate(20);
        $transaksi = DB::table('transaksi')->paginate();

        //data for chart
        // $categories = [];

        // foreach ($kategori_barang as $kb) {
        //     $categories[] = $kb->nama_kategori;
        // }

        // dd($categories);

        $data = array(
            'product'=>$products,
            'user'=>$users,
            'inboxes'=>$inboxes,
            'transaksi' => $transaksi,
            // 'categories'=>$categories,
        );
        return view('admin/Dashboard',$data);
    }


    public function logout(){
        Auth::logout();

        return redirect()->route('login');
    }


    public function Inbox()
    {
        $inboxes = DB::table('inbox')->paginate(20);
        return view('admin.Inbox.inbox',['inboxes'=>$inboxes]);
    }



}
