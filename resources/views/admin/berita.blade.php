@extends('layouts.navbar')
@section('title')
    <title>Berita</title>
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ribbons</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Ribbons</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title mt-2">Ribbons</h3>
                <div class="card-tools">
                <a href="{{route('berita_create')}}">
                    <button type="button" class="btn btn-block btn-primary btn-sm">Create Berita</button>
                </a>
            </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                @foreach ($news as $berita)
                  <div class="col-sm-4 mt-4">
                    <div class="position-relative p-3 bg-gray" style="height: 180px ;">
                      <div class="ribbon-wrapper">
                        <div class="ribbon bg-primary">
                            <div class="card-tools">
                                <a href="{{route('berita.edit',['id'=> $berita->id])}}">
                                    <button type="button" class="btn btn-tool" title="Collapse">
                                        <i class="fas fa-edit"></i></button>
                                </a>
                                <a href="{{route('berita.delete',['id'=> $berita->id])}}">
                                    <button type="button" class="btn btn-tool" title="Remove">
                                        <i class="fas fa-times"></i></button>
                                </a>
                            </div>
                        </div>
                      </div>
                      <div class="">{{$berita->title}}</div>
                      <br/>
                      <small>{!! \Illuminate\Support\Str::words($berita->description, 10, '...')!!}</small>
                    </div>
                    <a href="{{route('berita.read',['id'=>$berita->id])}}">
                        <button type="button" class="btn btn-block btn-primary btn-flat">Detail Berita</button>
                    </a>
                  </div>
                @endforeach
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
