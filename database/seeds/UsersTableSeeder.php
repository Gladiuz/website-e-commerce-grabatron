<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'email' => 'Admin123@mail.com',
            'password' => bcrypt('123456789'),
            'username' => 'Admin',
            'name_user' => 'Admin123',
            'level' => 'Admin'
        ]);
    }
}
