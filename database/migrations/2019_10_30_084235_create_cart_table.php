<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->increments('id_cart');
            $table->unsignedInteger('id_product');
            $table->unsignedInteger('id_user');
            $table->timestamps();
        });

        Schema::table('cart', function (Blueprint $table) {
            $table->foreign('id_product')
            ->references('id_product')
            ->on('product')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });

        Schema::table('cart', function (Blueprint $table) {
            $table->foreign('id_user')
            ->references('id')
            ->on('users')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
