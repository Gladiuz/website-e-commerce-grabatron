<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_product extends Model
{
    //
    protected $table = 'order_product';

    protected $fillable = [
        'id_order','id_product','quantity'
    ];

    public function order()
    {
        return $this->belongsTo('App\order','foreign_key','other_key');
    }

    public function product()
    {
        return $this->belongsTo('App\product','foreign_key','other_key');
    }

}
