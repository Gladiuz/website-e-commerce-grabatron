<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomepageController@getIndex')->name('home');
Route::get('/Shop', 'HomepageController@showShop')->name('page.shop');
Route::get('/Blog', 'HomepageController@showBlog')->name('page.blog');
Route::get('/Blog/read/{id}', 'HomepageController@showSingleBlog')->name('page.singleBlog');
Route::get('/Shop/products/{id}', 'HomepageController@showSingleProduct')->name('page.singleProduct');
Route::get('/About Us', 'HomepageController@showAboutUs')->name('page.about');

Route::get('/Register', 'RegisterController@create')->name('Register');
Route::post('/Register', 'RegisterController@Doregister')->middleware('guest');

Auth::routes();

Route::get('/Dashboard', 'HomeController@index')->name('Dashboard');

// Auth::routes();

Route::get('/Dashboard', 'HomeController@index')->name('Dashboard');
Route::get('/logout', 'HomeController@logout')->name('logout_Dashboard');
Route::get('/logoutUser', 'HomepageController@logoutUser')->name('logout.user');

Route::get('/banner', 'bannerController@index')->name('banner');
// Route::get('/logout', 'bannerController@logout')->name('logout_banner');
Route::post('/banner/create/make', 'bannerController@createBanner')->name('banner.create');
Route::get('/banner/create', 'bannerController@indexs')->name('create');
Route::get('/banner/edit/{id}', 'bannerController@edit')->name('banner.edit');
Route::post('/banner/edit/{id}/update', 'bannerController@update')->name('banner.update');
Route::get('/banner/delete/{id}', 'bannerController@delete')->name('banner.delete');

Route::get('/berita', 'beritaController@index')->name('berita');
Route::get('/berita/create', 'beritaController@indexs')->name('berita_create');
Route::get('/berita/edit/{id}', 'beritaController@edit')->name('berita.edit');
Route::post('/berita/edit/{id}/update', 'beritaController@update')->name('berita.update');
Route::post('/berita/create/make', 'beritaController@createBerita')->name('berita.make');
Route::get('/berita/delete/{id}', 'beritaController@delete')->name('berita.delete');
Route::get('/berita/read/{id}', 'beritaController@getfullberita')->name('berita.read');

Route::get('/about', 'aboutController@index')->name('about');
Route::get('/about/edit/{id}', 'aboutController@editAbout')->name('about.edit');
Route::post('/about/update/{id}', 'aboutController@updateAbout')->name('about.update');

Route::get('/produk', 'productController@index')->name('produk');
Route::get('/produk/create', 'productController@indexs')->name('produk.create');
Route::post('/produk/make', 'productController@createProduct')->name('produk.make');
Route::get('/produk/delete/{id}', 'productController@delete')->name('produk.delete');
Route::get('/produk/edit/{id}', 'productController@edit')->name('produk.edit');
Route::post('/produk/update/{id}', 'productController@update')->name('produk.update');

Route::get('/kategori', 'kategoriController@index')->name('kategori');
Route::get('/kategori/create', 'kategoriController@indexs')->name('kategori.create');
Route::post('/kategori/create/make', 'kategoriController@createKategori')->name('kategori.make');
Route::get('/kategori/delete/{id}', 'kategoriController@delete')->name('kategori.delete');
Route::get('/kategori/edit/{id}', 'kategoriController@edit')->name('kategori.edit');
Route::post('/kategori/update/{id}', 'kategoriController@update')->name('kategori.update');

Route::get('/Users', 'userController@index')->name('Users');
Route::get('/User/edit/{id}', 'userController@edit')->name('Users.edit');
Route::post('/User/update/{id}', 'userController@updateUser')->name('Users.update');

Route::get('/Profile/{id}', 'profileController@showProfile')->name('profile.index');
Route::post('/Profile/edit/{id}', 'profileController@updateProfile')->name('profile.update');


Route::get('/Shop/Search', 'HomepageController@searchProduct')->name('searching');
Route::get('/Shop/filter/{id}', 'filterController@filter')->name('filter.categories');

Route::get('/Cart', 'cartController@showCart')->name('page.cart');
Route::get('Cart-add/{id}', 'cartController@insertCart')->name('add.cart');
Route::post('update-cart', 'cartController@updateCart');
Route::get('/Cart/AllDelete', 'cartController@deleteAllCart')->name('delete.all');
Route::delete('remove-cart', 'cartController@deleteCart');

Route::get('/Inbox', 'HomeController@Inbox')->name('inbox');
Route::get('/Inbox/{id}', 'inboxController@readInbox')->name('read.inbox');
Route::get('/Inbox/delete/{id}', 'inboxController@deleteInbox')->name('delete.inbox');
Route::post('/contact/add', 'inboxController@createInbox')->name('inbox.added');

Route::get('/Checkout', 'checkoutController@index')->name('checkout');
Route::get('/province/{id}/cities', 'checkoutController@getCities');
Route::post('/Checkout/submit', 'checkoutController@submit')->name('checkout-submit');
Route::post('/Checkout/shipping', 'checkoutController@checkOut')->name('ongkir');

Route::get('/Shipping', 'ShippingController@index')->name('shipping');
Route::get('/Shipping/Transaction', 'ShippingController@create')->name('transaksi');

Route::get('/Transaksi', 'TransactionController@index')->name('transaction');
Route::post('/Transaksi/save', 'TransactionController@create')->name('transaksi.save');

Route::get('/Transaksi_history', 'HistoryController@index')->name('transaksi.page');
Route::get('/Transaksi_history/{id}', 'HistoryController@show')->name('transaksi.detail');
