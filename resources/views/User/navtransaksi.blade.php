<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    @yield('style')
        {{-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css"> --}}
        <link rel="stylesheet" href="{{asset('admin/by_myself/custom.css')}}">
        {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> --}}

    @yield('title')
</head>
<body>
    <nav class="main_nav">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="main_nav_content d-flex flex-row">
                        <!-- Main Nav Menu -->
                        {{-- <p class="m-3">a</p> --}}
                        <div class="cat_menu_container">
                            <div class="cat_menu_title d-flex flex-row align-items-center justify-content-start back">
                                <div class=""><i class="fas fa-long-arrow-alt-left" style="color:white;"></i></div>
                                <div class="cat_menu_text">Back</div>
                            </div>
                        </div>

                        <div class="main_nav_menu ml-auto">
                            <ul class="standard_dropdown main_nav_dropdown">
                                <li class="p-3 mb-2 bg-transaction text-white">Shipping</li>
                                ----->
                                <li class="p-3 mb-2 bg-transparent ">CheckOut</li>
                                {{-- <li class="hassubs">
                                    <a href="#">Super Deals<i class="fas fa-chevron-down"></i></a>
                                    <ul>
                                        <li>
                                            <a href="#">Menu Item<i class="fas fa-chevron-down"></i></a>
                                            <ul>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                    </ul>
                                </li>
                                <li class="hassubs">
                                    <a href="#">Featured Brands<i class="fas fa-chevron-down"></i></a>
                                    <ul>
                                        <li>
                                            <a href="#">Menu Item<i class="fas fa-chevron-down"></i></a>
                                            <ul>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                    </ul>
                                </li> --}}
                                {{-- <li class="hassubs"> --}}
                                    {{-- <a href="{{route('page.shop')}}" class="text-decoration-none">Shop<i class="fas"></i></a> --}}
                                    {{-- <ul>
                                        <li><a href="{{route('page.shop')}}">Shop<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="product.html">Product<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="">Blog<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="blog_single.html">Blog Post<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="regular.html">Regular Post<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="cart.html">Cart<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="contact.html">Contact<i class="fas fa-chevron-down"></i></a></li>
                                    </ul> --}}
                                {{-- </li> --}}
                                {{-- <li><a href="{{route('page.blog')}}" class="text-decoration-none">Blog<i class="fas fa-chevron-down"></i></a></li> --}}
                                {{-- <li><a href="{{route('page.about')}}" class="text-decoration-none">About<i class="fas fa-chevron-down"></i></a></li> --}}
                            </ul>
                        </div>

                        <!-- Menu Trigger -->

                        <div class="menu_trigger_container ml-auto">
                            <div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
                                <div class="menu_burger">
                                    <div class="menu_trigger_text">menu</div>
                                    <div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </nav>
    @yield('content')
</body>
<script src="{{asset('js/app.js')}}"></script>
{{-- <script src="{{asset('admin/by_myself/jquery-3.3.1.min.js')}}"></script> --}}
<script src="{{asset('user/styles/bootstrap4/popper.js')}}"></script>
<script src="{{asset('user/styles/bootstrap4/bootstrap.min.js')}}"></script>
<script src="{{asset('user/plugins/greensock/TweenMax.min.js')}}"></script>
<script src="{{asset('user/plugins/greensock/TimelineMax.min.js')}}"></script>
<script src="{{asset('user/plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
<script src="{{asset('user/plugins/greensock/animation.gsap.min.js')}}"></script>
<script src="{{asset('user/plugins/greensock/ScrollToPlugin.min.js')}}"></script>
<script src="{{asset('user/plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
<script src="{{asset('user/plugins/slick-1.8.0/slick.js')}}"></script>
<script src="{{asset('user/plugins/easing/easing.js')}}"></script>
<script src="{{asset('user/plugins/Isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('user/plugins/jquery-ui-1.12.1.custom/jquery-ui.js')}}"></script>
<script src="{{asset('user/plugins/parallax-js-master/parallax.min.js')}}"></script>
<script>
$(document).ready(function(){

    $('.back').click(function(e){
        var back = confirm("Apakah kamu yakin ingin kembali?");
        window.history.back(back);
        if (back == true) {

        }
    });

});
</script>

@yield('script')
</html>
