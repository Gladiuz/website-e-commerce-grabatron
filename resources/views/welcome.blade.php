@extends('User.navbar')
@section('title')
    <title>Grabatron</title>
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{asset('user/styles/bootstrap4/bootstrap.min.css')}}">
    <link href="{{asset('user/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/plugins/slick-1.8.0/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/styles/main_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/styles/responsive.css')}}">
@endsection

@section('content')
<div class="banner_2">
        <div class="banner_2_background" style="background-image:url(user/img/banner_2_background.jpg)"></div>
        <div class="banner_2_container">
            <div class="banner_2_dots"></div>
            <!-- Banner 2 Slider -->

            <div class="owl-carousel owl-theme banner_2_slider">

                <!-- Banner 2 Slider Item -->
                @foreach ($banners as $banner)
                <div class="owl-item">
                    <div class="banner_2_item">
                        <div class="container fill_height">
                            <div class="row fill_height">
                                <div class="col-lg-4 col-md-6 fill_height">
                                    <div class="banner_2_content">
                                        <div class="banner_2_title">{{$banner->title}}</div>
                                        <div class="banner_2_text">{!! \Illuminate\Support\Str::words($banner->description, 25, '...')!!}</div>
                                        <div class="rating_r rating_r_4 banner_2_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                        <div class="button banner_2_button"><a href="{{route('page.shop')}}">Explore</a></div>
                                    </div>

                                </div>
                                <div class="col-lg-8 col-md-6 fill_height">
                                    <div class="banner_2_image_container">
                                        <div class="banner_2_image"><img src="{{asset('css/img/'. $banner->thumbnail)}}" alt=""></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
                <div class="characteristics">
                        <div class="container">
                            <div class="row">

                                <!-- Char. Item -->
                                <div class="col-lg-3 col-md-6 char_col">

                                    <div class="char_item d-flex flex-row align-items-center justify-content-start">
                                        <div class="char_icon"><img src="{{asset('user/img/char_1.png')}}" alt=""></div>
                                        <div class="char_content">
                                            <div class="char_title">Free Delivery</div>
                                            <div class="char_subtitle">from $50</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Char. Item -->
                                <div class="col-lg-3 col-md-6 char_col">

                                    <div class="char_item d-flex flex-row align-items-center justify-content-start">
                                        <div class="char_icon"><img src="{{asset('user/img/char_2.png')}}" alt=""></div>
                                        <div class="char_content">
                                            <div class="char_title">Free Delivery</div>
                                            <div class="char_subtitle">from $50</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Char. Item -->
                                <div class="col-lg-3 col-md-6 char_col">

                                    <div class="char_item d-flex flex-row align-items-center justify-content-start">
                                        <div class="char_icon"><img src="{{asset('user/img/char_3.png')}}" alt=""></div>
                                        <div class="char_content">
                                            <div class="char_title">Free Delivery</div>
                                            <div class="char_subtitle">from $50</div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Char. Item -->
                                <div class="col-lg-3 col-md-6 char_col">

                                    <div class="char_item d-flex flex-row align-items-center justify-content-start">
                                        <div class="char_icon"><img src="{{asset('user/img/char_4.png')}}" alt=""></div>
                                        <div class="char_content">
                                            <div class="char_title">Free Delivery</div>
                                            <div class="char_subtitle">from $50</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    	<!-- Hot New Arrivals -->

	<div class="new_arrivals" >
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="tabbed_container">
                            <div class="tabs clearfix tabs-right">
                                <div class="new_arrivals_title">Hot New Arrivals</div>
                                <ul class="clearfix">
                                    <li class="active">Featured</li>
                                    {{-- <li>Audio & Video</li> --}}
                                    {{-- <li>Laptops & Computers</li> --}}
                                </ul>
                                <div class="tabs_line"><span></span></div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12" style="z-index:1;">

                                    <!-- Product Panel -->
                                    <div class="product_panel panel active">
                                        <div class="arrivals_slider slider">

                                            <!-- Slider Item -->
                                            @foreach ($products as $product)

                                            <div class="arrivals_slider_item">
                                                {{-- <form action="{{route('add.cart')}}" method="post" enctype="multipart/form-data"> --}}
                                                    {{ csrf_field() }}
                                                    <div class="border_active"></div>
                                                        <div class="product_item is_new d-flex flex-column align-items-center justify-content-center text-center">
                                                            <a class="wawawi" href="{{route('page.singleProduct',['id_product'=>$product->id_product])}}" tabindex="0" class="">
                                                                <div class="product_image d-flex flex-column align-items-center justify-content-center"><img src="{{asset('css/img/'. $product->gambar_product)}}" alt=""></div>
                                                            </a>
                                                                <div class="product_content">
                                                                        <div class="product_price">{{App\product::konversi($product->harga)}}</div>
                                                                        <div class="product_name"><div><a href="{{route('page.singleProduct',['id_product'=>$product->id_product])}}">{!! \Illuminate\Support\Str::words($product->nama_barang, 3, '...')!!}</a></div></div>
                                                                </div>
                                                                <div class="product_extras">
                                                                    {{-- <div class="product_color">
                                                                        <input type="radio" checked name="product_color" style="background:#b19c83">
                                                                        <input type="radio" name="product_color" style="background:#000000">
                                                                        <input type="radio" name="product_color" style="background:#999999">
                                                                    </div> --}}

                                                                        {{-- <input type="hidden" name="id_product"> --}}
                                                                        {{-- <input type="hidden" value="{{$product->id_product}}" name="id_product"> --}}
                                                                        <a class="btn product_cart_button add-to-cart" href="{{url('Cart-add/'. $product->id_product)}}" data-id="{{$product->id_product}}" role="button">Add to Cart</a>

                                                                </div>
                                                        </div>
                                                        <div class="product_fav"><i class="fas fa-heart"></i></div>
                                                        <ul class="product_marks">
                                                            <li class="product_mark product_discount">-25%</li>
                                                            <li class="product_mark product_new">new</li>
                                                        </ul>
                                                    </form>
                                            </div>
                                                @endforeach
                                        </div>
                                    </div>
                                </div>
                                {{-- <div class="arrivals_slider_dots_cover"></div> --}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
    </div>
@endsection

@section('script')
    <script src="{{asset('admin/js/custom.js')}}"></script>
    <script src="{{asset('admin/by_myself/jQuery.SimpleCart.js')}}"></script>
    {{-- <script src="{{asset('admin/by_myself/test.js')}}"></script> --}}
    <script type="text/javascript">
        $(".add-to-cart").click(function (e) {
                e.preventDefault();

                var ele = $(this);

                // ele.siblings('.btn-loading').show();

                $.ajax({
                    url: '{{ url('Cart-add') }}' + '/' + ele.attr("data-id"),
                    method: "get",
                    data: {_token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function (response) {
                        // window.location.reload();
                        // ele.siblings('.btn-loading').hide();
                        // alert("echo 3 freeze");
                        $(".hi").html(response.jumlah);

                        // $("span#status").html('<div class="alert alert-success">'+response.msg+'</div>');
                        // $("#header-bar").html(response.data);
                    },
                    error: function(){
                        alert("gagal memasukan");
                    }
                });
            });
    </script>

@endsection
