<?php

namespace App\Http\Controllers;

use App\order_product;
use App\transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // dd($id_order = $request->input('id_order'));

        $products = DB::table('product')->paginate();
        $users = DB::table('users')->paginate();
        $kategori_barang = DB::table('kategori_barang')->get();
        $inboxes = DB::table('inbox')->paginate(20);
        $transaksi = DB::table('transaksi')
        ->join('detail_order','transaksi.id_detail','=','detail_order.id_detail')
        ->join('order','detail_order.id_order', '=', 'order.id_order')
        ->join('users', 'order.id_user', '=', 'users.id')
        // ->where('detail_order.id_order', '=', $id_order)
        // ->join('order_product', 'order.id_order', '=', 'order_product.id_order')
        ->get();
        //

        $data = array(
            'product'=>$products,
            'user'=>$users,
            'inboxes'=>$inboxes,
            'transaksi' => $transaksi,
            // 'id_order' => $id_order
            // 'categories'=>$categories,
        );
        return view('admin/transaksi/transaksi',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        // dd('id_order');
        // dd($id_order = $request->input('id_order'));



        $order = DB::table('order')
        ->join('users','order.id_user','=','users.id')
        ->join('order_product','order.id_order','=','order_product.id_order')
        ->join('detail_order','order.id_order','=','detail_order.id_order')
        ->join('transaksi','transaksi.id_detail','=','detail_order.id_detail')
        ->where('transaksi.id_transaksi', '=', $id)
        ->first();

        // dd($order);

        $order_product = DB::table('order_product')
        ->join('order', 'order_product.id_order', '=', 'order.id_order')
        ->join('product', 'order_product.id_product', '=', 'product.id_product')
        ->where('order_product.id_order', '=', $order->id_order)
        ->get();

        $transaksi = DB::table('transaksi')
        ->join('detail_order','transaksi.id_detail','=','detail_order.id_detail')
        ->join('order','detail_order.id_order', '=', 'order.id_order')
        ->join('users', 'order.id_user', '=', 'users.id')
        ->join('order_product', 'order.id_order', '=', 'order_product.id_order')
        ->join('product', 'order_product.id_product', '=', 'product.id_product')
        ->where('transaksi.id_transaksi', '=', $id)
        ->orWhere('order_product.id_order', '=' ,'order.id_order')
        ->first();

        $data = array(
            'transaksi' => $transaksi,
            'order_product' => $order_product,
            // 'product' => $value,
        );

        return view('admin/transaksi/transaksi_detail', $data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
