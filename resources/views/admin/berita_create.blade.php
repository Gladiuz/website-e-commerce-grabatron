@extends('layouts.navbar')
@section('title')
    <title>Berita Create</title>
@endsection
@section('content')
   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Berita Create</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Berita</li>
              <li class="breadcrumb-item active">Berita Create</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <form action="{{route('berita.make')}}" method="POST" enctype="multipart/form-data" class="form">
                {{ csrf_field() }}
                <div class="card-header">
                    <h3 class="card-title mt-2">
                        Berita Create
                    </h3>
                    <!-- tools box -->
                    <div class="card-tools">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <!-- /. tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body pad">
                    <div class="mb-3">
                        <div class="form-group">
                            <label for="exampleInputTitle">Title</label>
                            <input type="text" class="form-control" name="title" id="exampleInputTitle" placeholder="Masukan Title">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputTitle">Deskripsi</label>
                            <textarea class="textarea" name="description" placeholder="Place some text here"
                            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Input Thumbnail</label>
                            <input type="file" name="thumbnail" class="form-control-file" id="exampleFormControlFile1">
                        </div>
                        {{-- <div class="form-group">
                            <label for="exampleInputTitle">Title</label>
                            <input type="file" class="custom-file-input" name="thumbnail" id="exampleInputFile">
                        </div> --}}

                    </div>
                </div>
            </form>
          </div>
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
