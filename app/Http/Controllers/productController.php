<?php

namespace App\Http\Controllers;

use App\kategori;
use App\product;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class productController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }


    public function index(Request $request)
    {
        if ($request->has('searching')) {
            $products = product::where('nama_barang','LIKE','%'.$request->searching.'%')->leftjoin('kategori_barang','product.id_kategori','=','kategori_barang.id_kategori')->paginate(5);
            $inboxes = DB::table('inbox')->paginate();
        }
        else {
            $products = DB::table('product')->leftjoin('kategori_barang','product.id_kategori','=','kategori_barang.id_kategori')->get();
            $inboxes = DB::table('inbox')->paginate();

            // $categories = DB::table('kategori_barang')->get();
        }
        $categories = DB::table('kategori_barang')->get();
        $data = array(
            'categories' => $categories,
            'products' => $products,
            'inboxes' => $inboxes,
        );

        return view('admin.produk.product', $data);
    }

    public function indexs()
    {
        $categories = kategori::all();
        $inboxes = DB::table('inbox')->paginate();
        return view('admin.produk.product_create', compact('categories'),['inboxes'=>$inboxes]);
    }


    public function createProduct(Request $request)
    {

        $this->validate($request,[
            'nama_barang' => 'required|min:3',
            'deskripsi' => 'required',
            'id_kategori' => 'required',
            'quantity_product' => 'required|numeric',
            'harga' => 'required|numeric',
            'gambar_product' => 'required|image|mimes:jpeg,png,gif,svg',
        ]);

        if ($request->hasFile('gambar_product')) {
            $gambar = $request->file('gambar_product');
            $gambarname = time() . '.' . $gambar->getClientOriginalExtension();
            Image::make($gambar)->save('css/img/'.$gambarname);
        }

        $products = product::create(array(
            'nama_barang' => Input::get('nama_barang'),
            'deskripsi' => Input::get('deskripsi'),
            'id_kategori' => request('id_kategori'),
            'quantity_product' => Input::get('quantity_product'),
            'harga' => Input::get('harga'),
            'gambar_product' => $gambarname,
        ));

        $products->save();
        return redirect()->route('produk')->with('success','Product has been successfully added!');
    }
    public function edit($id)
    {
        $product = DB::table('product')
        ->leftjoin('kategori_barang','product.id_kategori','=','kategori_barang.id_kategori')
        ->where('product.id_product', '=', $id)
        ->first();
        $categories = DB::table('kategori_barang')
        // ->leftJoin('product','kategori_barang.id_kategori','=','product.id_kategori')
        ->get();
        $inboxes = DB::table('inbox')->paginate();

        $data = array(
            'product' => $product,
            'inboxes' => $inboxes,
            'categories' => $categories,
        );

        return view('admin.produk.product_edit',$data);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'nama_barang' => 'required|min:3',
            'deskripsi' => 'required',
            'id_kategori' => 'required',
            'quantity_product' => 'required|numeric',
            'harga' => 'required|numeric',
        ]);

        $products = product::find($id);
        $products->nama_barang = $request->nama_barang;
        $products->deskripsi = $request->deskripsi;
        $products->harga = $request->harga;
        $products->id_kategori = $request->id_kategori;
        $products->quantity_product = $request->quantity_product;


        if ($request->hasFile('gambar_product')) {
            $gambar = $request->file('gambar_product');
            $extension = $gambar->getClientOriginalExtension();
            $gambarname = time() . '.' . $extension;
            $gambar->move('css/img/' ,$gambarname);
            $products->gambar_product = $gambarname;
        }

        $products->update();

        return redirect('produk')->with('success','Product has been updated successfully');
    }

    // public function delete($id)
    // {
    //     $products = product::find($id);
    //     $products->delete('$products');
    //     return redirect('produk');
    // }

    public function delete($id)
    {
        $products = product::find($id);
        $products->delete($products);
        return redirect('produk')->with('success','Product has been deleted successfully');
    }
}
