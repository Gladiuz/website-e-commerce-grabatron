<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detail_order extends Model
{
    //
    protected $table = 'detail_order';

    protected $fillable = [
        'id_order','alamat','kode_pos','phone'
    ];

    public function order()
    {
        return $this->belongsTo('App\order','foreign_key','other_key');
    }

    public function transaksi()
    {
        return $this->hasOne('App\transaksi');
    }
}
