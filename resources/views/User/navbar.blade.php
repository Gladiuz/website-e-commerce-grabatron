<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('style')
    <!-- font -->
    {{-- <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css"> --}}
    <link rel="stylesheet" href="{{asset('admin/by_myself/custom.css')}}">
    <!-- DataTables -->
    {{-- <link rel="stylesheet" href="{{asset('Admin_page/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}"> --}}
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"> --}}


    @yield('title')
</head>
<body>
    <header class="header">
        <div class="top_bar">
                <div class="container">
                    <div class="row">
                        <div class="col d-flex flex-row">
                            @if (Auth::check())
                            <div class="top_bar_content_item">
                                <div class="top_bar_icon">
                                    <ul class="standard_dropdown top_bar_dropdown">
                                        <img src="{{url('css/img/'. Auth::user()->avatar)}}" class="rounded-circle img-avatar-user img-fluid img-responsive flex-row align-items-center justify-content-end" alt="">
                                        <li>
                                        <a href="#" class="wawawi text-decoration-none ">{{Auth::user()->email}}</a>
                                            <ul>
                                                <li><a href="{{route('profile.index',['user'=>Auth::user()->id])}}" class="text-decoration-none">Profile</a></li>
                                                <li><a href="{{ route('logout.user') }}" class="text-decoration-none">Logout</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            @endif
                            <div class="top_bar_content ml-auto">
                                {{-- <div class="top_bar_menu">
                                    <ul class="standard_dropdown top_bar_dropdown">
                                        <li>
                                            <a href="#">English<i class="fas fa-chevron-down"></i></a>
                                            <ul>
                                                <li><a href="#">Italian</a></li>
                                                <li><a href="#">Spanish</a></li>
                                                <li><a href="#">Japanese</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Rupiah<i class="fas fa-chevron-down"></i></a>
                                            <ul>
                                                <li><a href="#">EUR Euro</a></li>
                                                <li><a href="#">GBP British Pound</a></li>
                                                <li><a href="#">JPY Japanese Yen</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div> --}}
                                <div class="top_bar_user">
                                    @if (!Auth::user())
                                    <div class="user_icon"><img src="{{asset('user/img/user.svg')}}" alt=""></div>
                                    <div><a href="{{route('register')}}" class="text-decoration-none">Register</a></div>
                                    <div><a href="{{route('login')}}" class="text-decoration-none">Sign in</a></div>
                                    @endif
                                    {{-- @if (Auth::user()->level == 'Admin')
                                        <p>test</p>
                                    @elseif(Auth::user()->level == 'User')
                                        <p>Bangke</p>
                                    @elseif(Auth::user()->level == 'guest')
                                        asd
                                    @endif --}}


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Header Main -->

            <div class="header_main">
                <div class="container">
                    <div class="row">

                        <!-- Logo -->
                        <div class="col-lg-2 col-sm-3 col-3 order-1">
                            <div class="logo_container">
                                <div class="logo ">
                                    <a href="{{route('home')}}" class="text-decoration-none">Grabatron</a></div>
                            </div>
                        </div>

                        <!-- Search -->
                        <div class="col-lg-6 col-12 order-lg-2 order-3 ">
                            <div class="header_search">
                                <div class="header_search_content">
                                    <div class="header_search_form_container">
                                        <form action="{{route('searching')}}" class="header_search_form clearfix">
                                            <input type="text" required="required" name="search_product" class="header_search_input" placeholder="Search for products...">
                                            <div class="custom_dropdown">
                                                <div class="custom_dropdown_list">
                                                    <span class="custom_dropdown_placeholder clc">All Categories</span>
                                                    <i class="fas fa-chevron-down"></i>
                                                    <ul class="custom_list clc">
                                                        @foreach ($categories as $kategori)
                                                            <li><a class="clc" href="#">{{$kategori->nama_kategori}}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <button type="submit" class="header_search_button trans_300" value="Submit"><img src="{{asset('user/img/search.png')}}" alt=""></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Wishlist -->
                        <div class="col-lg-4 col-9 order-lg-3 order-2 text-lg-left text-right">
                            <div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
                                <div class="wishlist d-flex flex-row align-items-center justify-content-end">
                                    <div class="wishlist_icon"><img src="{{asset('user/img/heart.png')}}" alt=""></div>
                                    <div class="wishlist_content">
                                        <div class="wishlist_text"><a href="#" class="text-decoration-none">Wishlist</a></div>
                                        <div class="wishlist_count">115</div>
                                    </div>
                                </div>

                                <!-- Cart -->
                                <div class="cart">
                                    <div class="cart_container d-flex flex-row align-items-center justify-content-end my-cart-icon">
                                        <a href="{{route('page.cart')}}">
                                        <div class="cart_icon">
                                            <img src="{{asset('user/img/cart.png')}}" alt="">
                                            <div class="cart_count">
                                                @if (session('cart'))
                                                <span class="hi">{{count(session('cart'))}}</span>

                                                @else
                                                <span class="hi">0</span>
                                                @endif
                                            </div>
                                        </div>
                                        </a>
                                        <div class="cart_content">
                                            <div class="cart_text"><a href="{{route('page.cart')}} " class="text-decoration-none">Cart</a></div>
                                            {{-- <div class="cart_price"></div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Main Navigation -->

            <nav class="main_nav">
                <div class="container">
                    <div class="row">
                        <div class="col">

                            <div class="main_nav_content d-flex flex-row">

                                <!-- Categories Menu -->

                                <div class="cat_menu_container">
                                    <div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
                                        <div class="cat_burger"><span></span><span></span><span></span></div>
                                        <div class="cat_menu_text">categories</div>
                                    </div>
                                    <ul class="cat_menu">
                                            @foreach ($categories as $kategori)
                                            <li><a href="{{route('filter.categories',['nama_kategori'=>$kategori->nama_kategori])}}" class="text-decoration-none" name="ini">{{$kategori->nama_kategori}}<i class="fas fa-chevron-right ml-auto"></i></a></li>
                                            @endforeach
                                    </ul>
                                </div>

                                <!-- Main Nav Menu -->

                                <div class="main_nav_menu ml-auto">
                                    <ul class="standard_dropdown main_nav_dropdown">
                                        <li><a href="{{route('home')}} " class="text-decoration-none">Home<i class="fas fa-chevron-down "></i></a></li>
                                        {{-- <li class="hassubs">
                                            <a href="#">Super Deals<i class="fas fa-chevron-down"></i></a>
                                            <ul>
                                                <li>
                                                    <a href="#">Menu Item<i class="fas fa-chevron-down"></i></a>
                                                    <ul>
                                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                            </ul>
                                        </li>
                                        <li class="hassubs">
                                            <a href="#">Featured Brands<i class="fas fa-chevron-down"></i></a>
                                            <ul>
                                                <li>
                                                    <a href="#">Menu Item<i class="fas fa-chevron-down"></i></a>
                                                    <ul>
                                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                        <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="#">Menu Item<i class="fas fa-chevron-down"></i></a></li>
                                            </ul>
                                        </li> --}}
                                        <li class="hassubs">
                                            <a href="{{route('page.shop')}}" class="text-decoration-none">Shop<i class="fas"></i></a>
                                            {{-- <ul>
                                                <li><a href="{{route('page.shop')}}">Shop<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="product.html">Product<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="">Blog<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="blog_single.html">Blog Post<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="regular.html">Regular Post<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="cart.html">Cart<i class="fas fa-chevron-down"></i></a></li>
                                                <li><a href="contact.html">Contact<i class="fas fa-chevron-down"></i></a></li>
                                            </ul> --}}
                                        </li>
                                        <li><a href="{{route('page.blog')}}" class="text-decoration-none">Blog<i class="fas fa-chevron-down"></i></a></li>
                                        <li><a href="{{route('page.about')}}" class="text-decoration-none">About<i class="fas fa-chevron-down"></i></a></li>
                                    </ul>
                                </div>

                                <!-- Menu Trigger -->

                                <div class="menu_trigger_container ml-auto">
                                    <div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
                                        <div class="menu_burger">
                                            <div class="menu_trigger_text">menu</div>
                                            <div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </nav>
                            <!-- Menu -->

                            <div class="page_menu">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col">

                                                <div class="page_menu_content">

                                                    <div  class="page_menu_search">
                                                        <form action="{{route('searching')}}">
                                                            <input type="search" required="required" name="search_product" class="page_menu_search_input" placeholder="Search for products...">
                                                        </form>
                                                    </div>
                                                    <ul class="page_menu_nav">
                                                        {{-- <li class="page_menu_item has-children">
                                                            <a href="#">Language<i class="fa fa-angle-down"></i></a>
                                                            <ul class="page_menu_selection">
                                                                <li><a href="#">English<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Italian<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Spanish<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Japanese<i class="fa fa-angle-down"></i></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="page_menu_item has-children">
                                                            <a href="#">Currency<i class="fa fa-angle-down"></i></a>
                                                            <ul class="page_menu_selection">
                                                                <li><a href="#">US Dollar<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">EUR Euro<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">GBP British Pound<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">JPY Japanese Yen<i class="fa fa-angle-down"></i></a></li>
                                                            </ul>
                                                        </li> --}}
                                                        <li class="page_menu_item">
                                                            <a href="{{route('home')}}">Home<i class="fa fa-angle-down"></i></a>
                                                        </li>
                                                        <li class="page_menu_item">
                                                            <a href="{{route('page.shop')}}">Shop<i class="fa fa-angle-down"></i></a>
                                                        </li>
                                                        {{-- <li class="page_menu_item has-children">
                                                            <a href="#">Super Deals<i class="fa fa-angle-down"></i></a>
                                                            <ul class="page_menu_selection">
                                                                <li><a href="#">Super Deals<i class="fa fa-angle-down"></i></a></li>
                                                                <li class="page_menu_item has-children">
                                                                    <a href="#">Menu Item<i class="fa fa-angle-down"></i></a>
                                                                    <ul class="page_menu_selection">
                                                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                                        <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                                    </ul>
                                                                </li>
                                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="page_menu_item has-children">
                                                            <a href="#">Featured Brands<i class="fa fa-angle-down"></i></a>
                                                            <ul class="page_menu_selection">
                                                                <li><a href="#">Featured Brands<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="page_menu_item has-children">
                                                            <a href="#">Trending Styles<i class="fa fa-angle-down"></i></a>
                                                            <ul class="page_menu_selection">
                                                                <li><a href="#">Trending Styles<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                                <li><a href="#">Menu Item<i class="fa fa-angle-down"></i></a></li>
                                                            </ul>
                                                        </li> --}}
                                                        <li class="page_menu_item"><a href="{{route('page.blog')}}">blog<i class="fa fa-angle-down"></i></a></li>
                                                        <li class="page_menu_item"><a href="{{route('page.about')}}">contact<i class="fa fa-angle-down"></i></a></li>
                                                    </ul>
                                                    @if (Auth::check())
                                                    <div class="menu_contact">
                                                        <div class="menu_contact_item"><div class="menu_contact_icon"><img src="images/mail_white.png" alt=""></div><a href="mailto:fastsales@gmail.com">{{Auth::user()->email}}</a></div>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </header>
            @yield('content')
            	<!-- Footer -->

	<footer class="footer">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 footer_col">
                        <div class="footer_column footer_contact">
                            <div class="logo_container">
                                <div class="logo"><a href="#">Grabatron</a></div>
                            </div>
                            <div class="footer_title">Got Question? Call Us 24/7</div>
                            <div class="footer_phone">+62 813 2051 8412</div>
                            <div class="footer_contact_text">
                                {{-- <p>17 Princess Road, London</p>
                                <p>Grester London NW18JR, UK</p> --}}
                            </div>
                            <div class="footer_social">
                                <ul>
                                    <li><a href="{{url('https://web.facebook.com/sinjz')}}"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="{{url('https://twitter.com/HasinBashari')}}"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    <li><a href="#"><i class="fab fa-google"></i></a></li>
                                    <li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-2 offset-lg-2">
                        <div class="footer_column">
                            <div class="footer_title">Find it Fast</div>
                            <ul class="footer_list">
                                @foreach ($categories as $kategori)
                                    <li><a href="{{route('filter.categories',['nama_kategori'=>$kategori->nama_kategori])}}" name="ini">{{$kategori->nama_kategori}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="footer_column">
                            <ul class="footer_list footer_list_2">
                                <li><a href="#">Video Games & Consoles</a></li>
                                <li><a href="#">Accessories</a></li>
                                <li><a href="#">Cameras & Photos</a></li>
                                <li><a href="#">Hardware</a></li>
                                <li><a href="#">Computers & Laptops</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="footer_column">
                            <div class="footer_title">Customer Care</div>
                            <ul class="footer_list">
                                @if (Auth::check())
                                <li><a href="{{route('profile.index',['user'=>Auth::user()->id])}}">My Account</a></li>
                                @endif
                                <li><a href="#">Order Tracking</a></li>
                                <li><a href="#">Wish List</a></li>
                                <li><a href="#">Customer Services</a></li>
                                <li><a href="#">Returns / Exchange</a></li>
                                <li><a href="#">FAQs</a></li>
                                <li><a href="#">Product Support</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </footer>
    </div>
            <script src="{{asset('js/app.js')}}"></script>
            {{-- <script src="{{asset('admin/by_myself/jquery-3.3.1.min.js')}}"></script> --}}
            <script src="{{asset('user/styles/bootstrap4/popper.js')}}"></script>
            <script src="{{asset('user/styles/bootstrap4/bootstrap.min.js')}}"></script>
            <script src="{{asset('user/plugins/greensock/TweenMax.min.js')}}"></script>
            <script src="{{asset('user/plugins/greensock/TimelineMax.min.js')}}"></script>
            <script src="{{asset('user/plugins/scrollmagic/ScrollMagic.min.js')}}"></script>
            <script src="{{asset('user/plugins/greensock/animation.gsap.min.js')}}"></script>
            <script src="{{asset('user/plugins/greensock/ScrollToPlugin.min.js')}}"></script>
            <script src="{{asset('user/plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
            <script src="{{asset('user/plugins/slick-1.8.0/slick.js')}}"></script>
            <script src="{{asset('user/plugins/easing/easing.js')}}"></script>
            <script src="{{asset('user/plugins/Isotope/isotope.pkgd.min.js')}}"></script>
            <script src="{{asset('user/plugins/jquery-ui-1.12.1.custom/jquery-ui.js')}}"></script>
            <script src="{{asset('user/plugins/parallax-js-master/parallax.min.js')}}"></script>

            <!-- DataTables -->
            <script src="{{asset('Admin_page/plugins/datatables/jquery.dataTables.js')}}"></script>
            <script src="{{asset('Admin_page/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

            @yield('script')

            {{-- <script src="{{asset('admin/js/custom.js')}}"></script> --}}
            {{-- <script src="{{asset('admin/js/shop_custom.js')}}"></script> --}}
            {{-- <script src="{{asset('admin/js/product_custom.js')}}"></script> --}}
            {{-- <script src="{{asset('admin/js/blog_single_custom.js')}}"></script> --}}
            {{-- <script src="{{asset('admin/js/blog_custom.js')}}"></script> --}}
            {{-- <script src="{{asset('admin/js/contact_custom.js')}}"></script> --}}
</body>
</html>
