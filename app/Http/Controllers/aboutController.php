<?php

namespace App\Http\Controllers;

use App\about;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class aboutController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $about = DB::table('about')->get();
        $inboxes = DB::table('inbox')->paginate();

        $data = array(
            'inboxes'=>$inboxes,
            'about'=> $about
        );
        return view('admin/about', $data);
    }

    public function editAbout($id)
    {
        $about = about::find($id);
        $inboxes = DB::table('inbox')->paginate();

        $data = array(
            'inboxes'=>$inboxes,
            'about'=> $about
        );
        return view('admin.about_edit',$data);
    }
    public function updateAbout(Request $request, $id)
    {
        $about = about::find($id);
        $about->deskripsi = $request->deskripsi;

        $about->update();
        return redirect('about');
    }
}
