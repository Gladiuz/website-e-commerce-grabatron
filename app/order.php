<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    //
    protected $table = 'order';
    protected $primaryKey = 'id_order';

    protected $fillable = [
        'id_user','asal_kota','tujuan_provinsi','tujuan_kota','kurir','berat'
    ];

    public function users()
    {
        return $this->belongsTo('App\users','foreign_key','other_key');
    }

    public function detail_order()
    {
        return $this->hasOne('App\detail_order');
    }

    public function order_product()
    {
        return $this->hasMany('App\order_product');
    }
}
