<?php

namespace App\Http\Controllers;

use App\inbox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class inboxController extends Controller
{
    public function createInbox(Request $request)
    {
        $this->validate($request,[
            'name_user' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
            'description' => 'required',
        ]);

        $inbox = inbox::create(array(
            'name_user' => Input::get('name_user'),
            'email' => Input::get('email'),
            'subject' => Input::get('subject'),
            'description' => Input::get('description'),
        ));

        $inbox->save();
        return redirect()->route('page.about')->with('success','Your message has been successfully sent');
    }

    public function readInbox($id)
    {
        $inboxes = DB::table('inbox')->paginate();
        $inbox = inbox::find($id);

        $data = array(
            'inboxes'=> $inboxes,
            'inbox'=> $inbox,
        );
        return view('admin.Inbox.inboxread', $data);
    }

    public function deleteInbox($id)
    {
        $inbox = inbox::find($id);
        $inbox->delete($inbox);
        return redirect()->route('inbox');
    }
}
