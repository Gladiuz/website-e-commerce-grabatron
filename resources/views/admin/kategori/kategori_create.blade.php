@extends('layouts.navbar')

@section('title')
    <title>create kategori</title>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Banner Create</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Banner</li>
              <li class="breadcrumb-item active">Banner Create</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Banner Create</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form  action="{{ route('kategori.make') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                  <div class="card-body">
                    <div class="form-group">
                      <label for="exampleInputTitle">Nama Kategori</label>
                      <input type="text" class="form-control" name="nama_kategori" id="exampleInputTitle" placeholder="Masukan Nama Kategori">
                    </div>
                    <div class="form-group">
                        <label class="" for="select1">Status</label>
                        {{-- <div class="col-md-9"> --}}
                          <select class="form-control" id="select1" name="status">
                            <option>Please select</option>
                            <option value="aktif">aktif</option>
                            <option value="nonaktif">nonaktif</option>
                          </select>
                        {{-- </div> --}}
                      </div>
                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection
