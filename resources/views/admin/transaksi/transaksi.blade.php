@extends('layouts.navbar')
@section('title')
    <title>Transaksi</title>
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Transaksi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Transaksi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Transaksi History</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th class="col-sm-1">#</th>
                  <th class="col-sm-2">id Order</th>
                  <th>User Pembeli</th>
                  <th>Total Belanja(Rp)</th>
                  <th>Tgl Transaksi</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($transaksi as $history)
                    <tr>
                        <input type="hidden" value="{{$history->id_order}}" name="id_order">
                        <td>{{$loop->iteration}}</td>
                        <td>{{$history->id_order}}</td>
                        <td>{{$history->name_user}}</td>
                        <td>{{App\product::konversi($history->total_belanja)}}</td>
                        <td>{{$history->created_at}}</td>
                        <td>
                            <a href="{{route('transaksi.detail',['id_transaksi' => $history->id_transaksi])}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-sticky-note"></i> Detail</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th class="col-sm-1">#</th>
                        <th>id Order</th>
                        <th>User Pembeli</th>
                        <th>Total Belanja(Rp)</th>
                        <th>Tgl Transaksi</th>
                        <th>Action</th>
                      </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection
