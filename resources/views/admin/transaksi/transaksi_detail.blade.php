@extends('layouts.navbar')
@section('title')
    <title>Transaksi</title>
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Transaksi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Transaksi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Transaksi History</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
            <div class="col-xl- col-lg-12 col-md-12 col-sm-12 col-12 padding">
                <div class="card">
                    <div class="card-header p-4">
                        <h3 class="pt-2 d-inline-block" href="index.html" data-abc="true">Invoice</h3>
                        <div class="float-right">

                            <input type="hidden" name="id_detail" value="{{$transaksi->id_detail}}">
                            <input type="hidden" name="id_order" value="{{$transaksi->id_order}}">
                            <h3 class="mb-0">Invoice #{{$transaksi->id_order}}</h3>
                            Date: {{ Carbon\Carbon::parse($transaksi->created_at)->format('j F,Y')  }}
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="col-sm-12 ">
                                {{-- <h5 class="mb-3">To:</h5> --}}
                                <h3 class="text-dark mb-1">{{$transaksi->name_user}}</h3>
                                <div>{{$transaksi->tujuan_provinsi}}, {{$transaksi->tujuan_kota}}, {{$transaksi->kode_pos}}</div>
                                <div>{{$transaksi->alamat}}</div>
                                <div>Email: {{$transaksi->email}}</div>
                                <div>Phone: {{$transaksi->phone}}</div>
                            </div>
                        </div>
                        <div class="table-responsive-sm">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="center">#</th>
                                        <th>Barang</th>
                                        {{-- <th>Description</th> --}}
                                        <th class="right">Harga</th>
                                        <th class="center">Qty</th>
                                        <th class="right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $total = 0 ?>
                                    @foreach ($order_product as $product)
                                    <?php $total += $product->harga * $product->quantity_order ?>
                                    <tr>
                                        {{-- <input type="hidden" name="product[{{$i}}][id_product]" value="{{$details['id_product']}}"> --}}
                                        {{-- <input type="hidden" value="{{$details['quantity']}}" name="product[{{$i}}][quantity]"> --}}
                                        <td class="center">{{ $loop->iteration }}</td>
                                        <td class="left strong">{!! \Illuminate\Support\Str::words($product->nama_barang, 3, '...')!!}</td>
                                        {{-- <td class="left">Iphone 10X with headphone</td> --}}
                                        <td class="right">{{App\product::konversi($product->harga)}}</td>
                                        <td class="center">{{$product->quantity_order}}</td>
                                        <td class="right">{{App\product::konversi($product->harga * $product->quantity_order)}}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-sm-5">

                            </div>
                            <div class="col-lg-4 col-sm-5 ml-auto">
                                <table class="table table-clear">
                                    <tbody>
                                        <tr>
                                            <td class="left">
                                                <strong class="text-dark">Subtotal :</strong>
                                            </td>
                                            <td class="right">{{App\product::konversi($total)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong class="text-dark">Ongkir :</strong>
                                            </td>
                                            {{-- <input type="hidden" value="{{$harga_total['ongkir']}}" name="ongkos_kirim"> --}}
                                            <td class="right">{{App\product::konversi($transaksi->ongkos_kirim)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong class="text-dark">Total :</strong> </td>
                                            <td class="right">
                                                {{-- <input type="hidden" value="{{$harga_total['total_order']}}" name="total_belanja"> --}}
                                                <strong class="text-dark">{{App\product::konversi($transaksi->total_belanja)}}</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection
