<?php

namespace App\Http\Controllers;

use App\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class profileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function showProfile()
    {
        $user = users::find(Auth::user()->id);
        $transaksi = DB::table('transaksi')
        ->join('detail_order','transaksi.id_detail','=','detail_order.id_detail')
        ->join('order','detail_order.id_order', '=', 'order.id_order')
        ->join('users', 'order.id_user', '=', 'users.id')
        ->where('users.id', '=', Auth::user()->id)
        ->get();
        $categories = DB::table('kategori_barang')->get();

        $data = array(
            'categories'=> $categories,
            'user' => $user,
            'transaksi' => $transaksi,
        );

        return view('profile', $data);
    }

    public function updateProfile(Request $request, $id)
    {
        $user = users::find($id);
        $this->validate($request,[
            'name_user' => 'required',
            'username' => 'required|min:3',
            'email' => 'required|email',
        ]);

        $user->name_user = $request->name_user;
        $user->email = $request->email;
        $user->username = $request->username;

        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $extension = $avatar->getClientOriginalExtension();
            $avatarname = time() . '.' . $extension;
            $avatar->move('css/img/' ,$avatarname);
            $user->avatar = $avatarname;
        }

        $user->save();

        return redirect('/Profile/'. $id)->with('success','Your profile has been updated successfully');

    }
}
