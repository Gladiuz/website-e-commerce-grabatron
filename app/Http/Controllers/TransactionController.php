<?php

namespace App\Http\Controllers;

use App\product;
use App\transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        // $id_order = $request->input('id_order');
        $harga_total = session()->get('tests');
        $detail_order = DB::table('detail_order')
        ->join('order','detail_order.id_order', '=', 'order.id_order')
        ->join('users', 'order.id_user', '=', 'users.id')
        ->join('order_product', 'order.id_order', '=', 'order_product.id_order')
        ->where('detail_order.id_order', '=', $harga_total['id_order'])
        ->orderBy('detail_order.created_at','DESC')
        ->first();

        $data = array(
            'harga_total' => $harga_total,
            'detail_order' => $detail_order,
        );
        return view('transaksi', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $transaksi = new transaksi();
        $transaksi->id_detail = Input::get('id_detail');
        $transaksi->total_belanja = Input::get('total_belanja');
        $transaksi->ongkos_kirim = Input::get('ongkos_kirim');
        // dd($request);

        // Quantity();
        foreach ($request->input('product') as $value) {
            // dd($value['quantity']);
            $product = product::where('id_product', $value['id_product'])->first();
            $product->quantity_product = $product->quantity_product - $value['quantity'];
            $product->update();
        }


        $transaksi->save();
        session()->forget('cart');

        return redirect()->route('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function Quantity(Request $request)
    {

    }
}
