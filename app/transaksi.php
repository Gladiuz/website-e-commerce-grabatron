<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
    //
    protected $table = 'transaksi';
    protected $primaryKey = 'id_transaksi';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT ='tgl_transaksi';

    protected $fillable = [
        'id_detail','total_belanja','ongkos_kirim','tgl_transaksi'
    ];

    public function detail_order()
    {
        return $this->belongsTo('App\detail_order','foreign_key','other_key');
    }
}
