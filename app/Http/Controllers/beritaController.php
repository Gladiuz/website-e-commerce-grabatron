<?php

namespace App\Http\Controllers;

use App\berita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class beritaController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }


    public function index(Request $request)
    {
        // $user = DB::table('users')->rightjoin('berita','users.id', '=', 'berita.author')->get();
        if ($request->has('searching')) {
            $news = berita::where('title','LIKE','%'.$request->searching.'%')->get();
            $inboxes = DB::table('inbox')->paginate();
        }
        else {
            $news = DB::table('berita') ->get();
            $inboxes = DB::table('inbox')->paginate();
        }

        $data = array(
            'news' => $news,
            'inboxes'=> $inboxes
        );
        return view('admin.berita', $data);
    }
    public function getfullberita($id)
    {
        $news = berita::find($id);
        $inboxes = DB::table('inbox')->paginate();

        $data = array(
            'berita' => $news,
            'inboxes'=> $inboxes
        );

        return view('admin.berita_full',$data);
    }

    public function indexs()
    {
        $inboxes = DB::table('inbox')->paginate();
        return view('/admin/berita_create',['inboxes'=>$inboxes]);
    }

    public function createBerita(Request $request)
    {

        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'thumbnail' => 'required',
        ]);

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailname= time() . '.' . $thumbnail->getClientOriginalName();
            Image::make($thumbnail)->save('css/img/'.$thumbnailname);
        }
        // $imgpath = request()->file('description')->store('uploads', 'public');
        $news = berita::create(array(
            'title' => Input::get('title'),
            'description' => $request['description'],
            'author' => Auth::user()->id,
            'thumbnail' => $thumbnailname,
        ));
        $news->save();
        return redirect()->route('berita')->with('success','Berita has been successfully added!');
    }

    public function edit($id)
    {
        $news = berita::find($id);
        $inboxes = DB::table('inbox')->paginate();

        $data = array(
            'berita'=> $news,
            'inboxes' => $inboxes
        );
        return view('admin.berita_edit',$data);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
        ]);

        // dd($request['description']);
        $news = berita::find($id);
        $news->title = $request->title;
        $news->description = $request->description;

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $extension = $thumbnail->getClientOriginalExtension();
            $thumbnailname = time() . '.' . $extension;
            $thumbnail->move('css/img/',$thumbnailname);
            $news->thumbnail = $thumbnailname;
        }

        $news->update();

        return redirect('berita')->with('success','Banner has been updated successfully');
    }
    public function delete($id)
    {
        $news = berita::find($id);
        $news->delete($news);
        return redirect('berita')->with('success','Banner has been deleted successfully');
    }
}
