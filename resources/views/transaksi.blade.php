<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>

    <!------ Include the above in your HEAD tag ---------->
    <form action="{{route('transaksi.save')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 padding">
            <div class="card">
                <div class="card-header p-4">
                    <h3 class="pt-2 d-inline-block" href="index.html" data-abc="true">Invoice</h3>
                    <div class="float-right">

                        <input type="hidden" name="id_detail" value="{{$detail_order->id_detail}}">
                        <h3 class="mb-0">Invoice #{{$detail_order->id_order}}</h3>
                        Date: {{ Carbon\Carbon::parse($detail_order->created_at)->format('j F,Y')  }}
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-12 ">
                            {{-- <h5 class="mb-3">To:</h5> --}}
                            <h3 class="text-dark mb-1">{{$detail_order->name_user}}</h3>
                            <div>{{$detail_order->tujuan_provinsi}}, {{$detail_order->tujuan_kota}}, {{$detail_order->kode_pos}}</div>
                            <div>{{$detail_order->alamat}}</div>
                            <div>Email: {{$detail_order->email}}</div>
                            <div>Phone: {{$detail_order->phone}}</div>
                        </div>
                    </div>
                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th>Barang</th>
                                    {{-- <th>Description</th> --}}
                                    <th class="right">Harga</th>
                                    <th class="center">Qty</th>
                                    <th class="right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $total = 0 ?>
                                <?php $i = 0 ?>
                                @foreach ((array) session('cart') as $id => $details)
                                <?php $total += $details['harga'] * $details['quantity'] ?>
                                <tr>
                                    <input type="hidden" name="product[{{$i}}][id_product]" value="{{$details['id_product']}}">
                                    <input type="hidden" value="{{$details['quantity']}}" name="product[{{$i}}][quantity]">
                                    <td class="center">{{ $loop->iteration }}</td>
                                    <td class="left strong">{!! \Illuminate\Support\Str::words($details['nama_barang'], 3, '...')!!}</td>
                                    {{-- <td class="left">Iphone 10X with headphone</td> --}}
                                    <td class="right">{{App\product::konversi($details['harga'])}}</td>
                                    <td class="center">{{ $details['quantity'] }}</td>
                                    <td class="right">{{App\product::konversi($details['harga'] * $details['quantity'])}}</td>
                                </tr>
                                <?php $i++ ?>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-5">

                        </div>
                        <div class="col-lg-4 col-sm-5 ml-auto">
                            <table class="table table-clear">
                                <tbody>
                                    <tr>
                                        <td class="left">
                                            <strong class="text-dark">Subtotal :</strong>
                                        </td>
                                        <td class="right">{{App\product::konversi($total)}}</td>
                                    </tr>
                                    <tr>
                                        <td class="left">
                                            <strong class="text-dark">Ongkir :</strong>
                                        </td>
                                        <input type="hidden" value="{{$harga_total['ongkir']}}" name="ongkos_kirim">
                                        <td class="right">{{App\product::konversi($harga_total['ongkir'])}}</td>
                                    </tr>
                                    <tr>
                                        <td class="left">
                                            <strong class="text-dark">Total :</strong> </td>
                                        <td class="right">
                                            <input type="hidden" value="{{$harga_total['total_order']}}" name="total_belanja">
                                            <strong class="text-dark">{{App\product::konversi($harga_total['total_order'])}}</strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </form>
</body>
</html>

