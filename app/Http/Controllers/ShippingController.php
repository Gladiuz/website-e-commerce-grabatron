<?php

namespace App\Http\Controllers;

use App\detail_order;
use App\order;
use App\order_product;
use App\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ShippingController extends Controller
{
    /**
     * parameter untuk yang sudah login
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = DB::table('kategori_barang')->get();
        $user = users::find(Auth::user()->id);
        $order = DB::table('order')
        ->join('users','order.id_user','=','users.id')
        // ->join('detail_order','order.id_order','=','detail_order.id_order')
        ->join('order_product','order.id_order','=','order_product.id_order')
        ->where('users.id', '=', Auth::user()->id)
        ->first();

        $harga_total = session()->get('tests');


        $data = array(
            'categories' => $categories,
            'user' => $user,
            'harga_total' => $harga_total,
            'order' => $order,
        );

        return view('shipping', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // $order = order::find($id);
        $detail_order = detail_order::create(array(
            'id_order' => Input::get('id_order'),
            'alamat' => Input::get('alamat'),
            'kode_pos' => Input::get('kode_pos'),
            'phone' => Input::get('phone'),
        ));


        // $harga_total = session()->get('tests');

        $detail_order->save();
        return redirect()->route('transaction');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
