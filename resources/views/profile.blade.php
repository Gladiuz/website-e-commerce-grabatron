@extends('User.navbar')
@section('title')
    <title>My Profile</title>
@endsection


@section('style')
{{-- <link rel="stylesheet" type="text/css" href="{{asset('user/styles/bootstrap4/bootstrap.min.css')}}"> --}}
    <link href="{{asset('user/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/plugins/slick-1.8.0/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/styles/main_styles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/styles/responsive.css')}}">
    <link href="{{asset('admin/node_modules/@coreui/icons/css/coreui-icons.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/node_modules/flag-icon-css/css/flag-icon.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/node_modules/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}
    <!-- Logo -->
    {{-- <link rel="icon" type="image/ico" href="{{asset('css/img/test.jpeg')}}" sizes="any" /> --}}

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('Admin_page/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('Admin_page/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('Admin_page/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->
    <link rel="stylesheet" href="{{asset('Admin_page/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('Admin_page/dist/css/adminlte.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('Admin_page/plugins/datatables-bs4/css/dataTables.bootstrap4.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('Admin_page/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('Admin_page/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('Admin_page/plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Icons-->
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> --}}
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  {{-- <div class="content-wrapper mt-5"> --}}
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content m-5">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class=" img-fluid img-circle"
                       src="{{asset('css/img/'. $user->avatar)}}"
                       alt="User profile picture">
                </div>

                {{-- <h3 class="profile-username text-center">{{$user->name_user}}</h3> --}}

                {{-- <p class="text-muted text-center">{{$user->email}}</p> --}}

                <ul class="list-group list-group-unbordered mb-3 mt-5">
                  <li class="list-group-item">
                    <b>Nama : </b> <a class="">{{$user->name_user}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Username : </b> <a class="">{{$user->username}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Email : </b> <a class="">{{$user->email}}</a>
                  </li>
                </ul>
                @if (Auth::user()->level == 'Admin')
                <a href="{{route('Dashboard')}}" class="btn btn-primary btn-block"><b>Admin Page</b></a>
                @endif
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li>
                  {{-- <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Timeline</a></li> --}}
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <div class="">
                        <h1>History Transaksi</h1>
                    </div>
                    <hr>
                    <table id="example4" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th class="col-sm-1">#</th>
                          <th class="col-sm-2">id Order</th>
                          <th>User Pembeli</th>
                          <th>Total Belanja(Rp)</th>
                          <th>Tgl Transaksi</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($transaksi as $history)
                            <tr>
                                {{-- <input type="hidden" value="{{$history->id_order}}" name="id_order"> --}}
                                <td>{{$loop->iteration}}</td>
                                <td>{{$history->id_order}}</td>
                                <td>{{App\product::konversi($history->ongkos_kirim)}}</td>
                                <td>{{App\product::konversi($history->total_belanja)}}</td>
                                <td>{{ Carbon\Carbon::parse($history->tgl_transaksi)->format('l, j M Y H:i A')  }}</td>
                                <td>
                                    <a href="{{route('transaksi.detail',['id_transaksi' => $history->id_transaksi])}}" class="btn btn-primary btn-sm">
                                        <i class="fa fa-sticky-note"></i> Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="col-sm-1">#</th>
                                <th>id Order</th>
                                <th>User Pembeli</th>
                                <th>Total Belanja(Rp)</th>
                                <th>Tgl Transaksi</th>
                                <th>Action</th>
                              </tr>
                        </tfoot>
                    </table>
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal" method="POST" action="{{route('profile.update',['id'=>$user->id])}}">
                        {{ csrf_field() }}
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName" name="name_user" value="{{$user->name_user}}" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputEmail" value="{{$user->email}}" name="email" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Username</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName2" name="username" value="{{$user->username}}" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  {{-- </div> --}}
  <!-- /.content-wrapper -->

@endsection

@section('script')
<!-- jQuery -->
<script src="{{asset('Admin_page/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery Mapael -->
<script src="{{asset('Admin_page/plugins/jquery-mousewheel/jquery.mousewheel.js')}}"></script>
<script src="{{asset('Admin_page/plugins/raphael/raphael.min.js')}}"></script>
<script src="{{asset('Admin_page/plugins/jquery-mapael/jquery.mapael.min.js')}}"></script>
<script src="{{asset('Admin_page/plugins/jquery-mapael/maps/usa_states.min.js')}}"></script>
 <!-- DataTables -->
<script src="{{asset('Admin_page/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('Admin_page/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
    $(function (){
       $("#example4").DataTable();
    });
   </script>

@endsection

