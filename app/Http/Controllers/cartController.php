<?php

namespace App\Http\Controllers;

use App\cart;
use App\product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

// use Nexmo\Response;

class cartController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }



    public function insertCart($id)
    {
        // $this->validate($request,[
        //     'id_product' => 'required',
        //     'id_user' => 'required',
        // ]);


        // $cart = new cart();
        // $product = new product();
            // $hi = $product->id_product = $request->input('id_product');
            // $cart->id_user = Auth::guest();
            // $minute = 60;
            // $response = new Response('Set Cookie');
            // $cookie = cookie('id-product', $hi, true ,$minute);
            // $cookie = Cookie::queue(Cookie::make('id-product', $hi , $minute));


            $product = product::find($id);

            if (!$product) {
                abort(404);
            }
            // $product = product::where('product.id_product', '=' , $id)
            // ->leftjoin('kategori_barang','product.id_kategori','=','kategori_barang.id_kategori')
            // ->first();
            // $categories = DB::table('kategori_barang')->get();

            $cart = session()->get('cart');

            // if cart is empty then this the first product
            if (!$cart) {
                $cart = [
                    $id => [
                        "id_product" => $product->id_product,
                        "nama_barang" => $product->nama_barang,
                        "quantity" => 1,
                        "harga" => $product->harga,
                        "gambar_product" => $product->gambar_product,
                        "id_kategori" => $product->id_kategori,
                    ]
                ];

                session()->put('cart', $cart);

                $jumlah = count(session('cart'));

                $htmlCart = view('cart');

                return response()->json(['data'=> $htmlCart, 'jumlah'=> $jumlah]);

                // return redirect()->back();
            }

            // if cart not empty then check if this product exist then increment quantity
            if (isset($cart[$id])) {

                $cart[$id]['quantity']++;

                session()->put('cart', $cart);

                $htmlCart = view('cart');

                $jumlah = count(session('cart'));

                return response()->json(['data'=> $htmlCart, 'jumlah'=> $jumlah]);

                // return redirect()->back();

            }

            // if item not exist in cart then add to cart with quantity = 1
            $cart[$id] = [
                "id_product" => $product->id_product,
                "nama_barang" => $product->nama_barang,
                "quantity" => 1,
                "harga" => $product->harga,
                "gambar_product" => $product->gambar_product,
                "id_kategori" => $product->id_kategori,
            ];

            session()->put('cart', $cart);

            $htmlCart = view('cart');

            $jumlah = count(session('cart'));

            return response()->json(['data'=> $htmlCart, 'jumlah'=> $jumlah]);

            // return redirect()->back();


        // return redirect()->route('home');
        // ->with($cookie);
    }




    public function showCart(Request $request)
    {


            $cart = session()->get('cart');
            // $s = json_decode($test);

            $product = DB::table('product')
            // ->join('cart', 'product.id_product','=','cart.id_product')
            // ->join('users','cart.id_user','=','users.id')
            // ->leftjoin('kategori_barang','product.id_kategori','=','kategori_barang.id_kategori')
            // ->where('product.id_kategori','=', $cart)
            ->get();
            $categories = DB::table('kategori_barang')->get();
            // $cart = DB::table('cart')
            //     ->join('users','cart.id_user','=','users.id')
            //     ->where('product.id_product','=', $test)
            //     ->paginate();


        $data = array(
            'products' => $product,
            'categories' => $categories,
            // 'cart' => $cart,
            // 'harga_total' => $harga_total,
            // 'test' => $test
            // 'sub_total' => $sub_total,
        );


        return view('cart', $data);
    }


    public function updateCart(Request $request)
    {
        if($request->id_product and $request->quantity){

            $cart = session()->get('cart');

            $cart[$request->id_product]['quantity'] = $request->quantity;

            session()->put('cart', $cart);

            $subTotal = product::konversi($cart[$request->id_product]['quantity'] * $cart[$request->id_product]['harga']);

            $total = $this->getCartTotal();

            $htmlCart = view('cart');

            return response()->json(['data'=> $htmlCart, 'total' => $total, 'subTotal' => $subTotal]);

        }
    }




    public function deleteAllCart()
    {
        // $cart = DB::table('cart')
        // ->join('users','cart.id_user','=','users.id')
        // ->where('users.id','=', Auth::user()->id)
        // ->delete();
        // $cookie = Cookie::forget('id-product');
        session()->forget('cart');
        return redirect()->route('page.cart');
    }

    public function deleteCart(Request $request)
    {
        if ($request->id_product) {
            $cart = session()->get('cart');

            if (isset($cart[$request->id_product])) {

                unset($cart[$request->id_product]);

                session()->put('cart', $cart);
            }

            $total = $this->getCartTotal();

            $htmlCart = view('cart');

            return response()->json(['data'=> $htmlCart, 'total' => $total]);
        }
    }


    /**
     * getCartTotal
     *
     *
     * @return float|int
     */
    public function getCartTotal()
    {
        $total = 0;

        $cart = session()->get('cart');

        foreach($cart as $id => $details) {

            $total += $details['harga'] * $details['quantity'];
        }

        return (product::konversi($total));
    }

}
