<?php

namespace App\Http\Controllers;

use App\kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class kategoriController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    //
    public function index(Request $request)
    {
        if ($request->has('searching')) {
            $categorys = kategori::where('nama_kategori','LIKE','%'.$request->searching.'%')->get();
            $inboxes = DB::table('inbox')->paginate();
        }
        else {
            $categorys = DB::table('kategori_barang')->get();
            $inboxes = DB::table('inbox')->paginate();
        }

        $data = array(
            'categorys'=>$categorys,
            'inboxes'=>$inboxes,
        );

        return view('admin.kategori.kategori',$data);
    }

    public function indexs()
    {
        $inboxes = DB::table('inbox')->paginate();
        return view('admin.kategori.kategori_create',['inboxes'=>$inboxes]);
    }

    public function createKategori(Request $request)
    {

        $this->validate($request,[
            'nama_kategori' => 'required',
            'status' => 'required|in:aktif,nonaktif',
        ]);

        $categorys = kategori::create(array(
            'nama_kategori' => Input::get('nama_kategori'),
            'status' => Input::get('status'),
        ));
        $categorys->save();
        return redirect()->route('kategori')->with('success','Berita has been successfully added!');
    }

    public function edit($id)
    {
        $kategori = kategori::find($id);
        $inboxes = DB::table('inbox')->paginate();

        $data = array(
            'kategori'=>$kategori,
            'inboxes'=>$inboxes,
        );
        return view('admin.kategori.kategori_edit',$data);
    }

    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'name_kategori' => 'in:aktif,nonaktif',
            'status' => 'required',
        ]);

        $kategori = kategori::find($id);
        $kategori->nama_kategori = $request->nama_kategori;
        $kategori->status = $request->status;

        $kategori->update();

        return redirect('kategori')->with('success','Kategori has been updated successfully');
    }

    public function delete($id)
    {
        $categorys = kategori::find($id);
        $categorys->delete($categorys);
        return redirect('kategori')->with('success','Kategori has been deleted successfully');
    }
}
