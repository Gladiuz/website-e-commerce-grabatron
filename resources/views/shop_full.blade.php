@extends('User.navbar')
@section('title')
    <title>{{$product->nama_barang}}</title>
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/bootstrap4/bootstrap.min.css')}}">
<link href="{{asset('user/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('user/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/plugins/jquery-ui-1.12.1.custom/jquery-ui.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/product_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/product_responsive.css')}}">
@endsection

@section('content')
    <!-- Single Product -->

	<div class="single_product">
		<div class="container">
			<div class="row">

				<!-- Images -->
				<div class="col-lg-2 order-lg-1 order-2">
					<ul class="image_list">
						<li data-image="{{asset('css/img/'. $product->gambar_product)}}"><img src="{{asset('css/img/'. $product->gambar_product)}}" alt=""></li>
						{{-- <li data-image="images/single_2.jpg"><img src="images/single_2.jpg" alt=""></li> --}}
						{{-- <li data-image="images/single_3.jpg"><img src="images/single_3.jpg" alt=""></li> --}}
					</ul>
				</div>

                <!-- Selected Image -->
				<div class="col-lg-5 order-lg-2 order-1">
					<div class="image_selected"><img src="{{asset('css/img/'. $product->gambar_product)}}" alt=""></div>
				</div>

				<!-- Description -->
				<div class="col-lg-5 order-3">
					<div class="product_description">
						<div class="product_category">{{$product->nama_kategori}}</div>
						<div class="product_name">{{$product->nama_barang}}</div>
                        <hr>
                        <div class="product_price">{{App\product::konversi($product->harga)}}</div>
						<div class="order_info d-flex flex-row">
								<div class="clearfix" style="z-index: 1000;">
                                    <!-- Product Quantity -->
									<div class="product_quantity clearfix">
										<span>Quantity:  {{($product->quantity_product)}}</span>
                                    </div>
										{{-- <input id="quantity_input" type="text" pattern="0-12" value="1"> --}}
										{{-- <div class="quantity_buttons">
											<div id="quantity_inc_button" class="quantity_inc quantity_control"><i class="fas fa-chevron-up"></i></div>
											<div id="quantity_dec_button" class="quantity_dec quantity_control"><i class="fas fa-chevron-down"></i></div>
										</div> --}}

									<!-- Product Color -->
									{{-- <ul class="product_color">
										<li>
											<span>Color: </span>
											<div class="color_mark_container"><div id="selected_color" class="color_mark"></div></div>
											<div class="color_dropdown_button"><i class="fas fa-chevron-down"></i></div>

											<ul class="color_list">
												<li><div class="color_mark" style="background: #999999;"></div></li>
												<li><div class="color_mark" style="background: #b19c83;"></div></li>
												<li><div class="color_mark" style="background: #000000;"></div></li>
											</ul>
										</li>
									</ul> --}}

                                </div>
                                {{-- <form action="" method="" enctype="multipart/form-data"> --}}
                                    {{-- {{ csrf_field() }} --}}
								{{-- <div class="button_container"> --}}
                                        {{-- <input type="hidden" value="{{$product->id_product}}" name="id_product"> --}}
                                        {{-- <button  class="button cart_button" type="submit">Add to Cart</button> --}}
                                        <a class="button cart_button add-to-cart" href="{{url('Cart-add/'. $product->id_product)}}" data-id="{{$product->id_product}}" role="button">Add to Cart</a>
                                    {{-- <div class="product_fav"><i class="fas fa-heart"></i></div> --}}
								{{-- </div> --}}
                                {{-- </form> --}}
                            </div>
                        </div>
                    </div>
            </div>
            <hr class="mt-5">
            <div class="card card-outline-secondary my-4">
                <div class="card-header">
                  Detail Product
                </div>
                <div class="card-body">
                    <p>{{$product->deskripsi}}</p>
                </div>
              </div>
		</div>
	</div>

	<!-- Recently Viewed -->

	<div class="viewed">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="viewed_title_container">
						<h3 class="viewed_title">Recently Viewed</h3>
						<div class="viewed_nav_container">
							<div class="viewed_nav viewed_prev"><i class="fas fa-chevron-left"></i></div>
							<div class="viewed_nav viewed_next"><i class="fas fa-chevron-right"></i></div>
						</div>
					</div>

					<div class="viewed_slider_container">

						<!-- Recently Viewed Slider -->

						<div class="owl-carousel owl-theme viewed_slider">

                            <!-- Recently Viewed Item -->
                            @foreach ($recent_products as $recent)
							<div class="owl-item">
								<div class="viewed_item discount d-flex flex-column align-items-center justify-content-center text-center">
									<div class="viewed_image"><img src="{{asset('css/img/'. $recent->gambar_product)}}" alt=""></div>
									<div class="viewed_content text-center">
										<div class="viewed_price">{{App\product::konversi($recent->harga)}}<span></span></div>
										<div class="viewed_name"><a href="{{route('page.singleProduct',['id_product'=>$recent->id_product])}}">{!! \Illuminate\Support\Str::words($recent->nama_barang, 3, '...')!!}</a></div>
									</div>
									<ul class="item_marks">
										<li class="item_mark item_discount">0%</li>
										<li class="item_mark item_new">new</li>
									</ul>
								</div>
                            </div>
                            @endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('script')
    <script src="{{asset('admin/js/product_custom.js')}}"></script>
    <script type="text/javascript">
        $(".add-to-cart").click(function (e) {
                e.preventDefault();

                var ele = $(this);

                // ele.siblings('.btn-loading').show();

                $.ajax({
                    url: '{{ url('Cart-add') }}' + '/' + ele.attr("data-id"),
                    method: "get",
                    data: {_token: '{{ csrf_token() }}'},
                    dataType: "json",
                    success: function (response) {
                        // window.location.reload();
                        // ele.siblings('.btn-loading').hide();
                        // alert("echo 3 freeze");
                        $(".hi").html(response.jumlah);

                        // $("span#status").html('<div class="alert alert-success">'+response.msg+'</div>');
                        // $("#header-bar").html(response.data);
                    },
                    error: function(){
                        alert("ammar bgst");
                    }
                });
            });
    </script>
@endsection
