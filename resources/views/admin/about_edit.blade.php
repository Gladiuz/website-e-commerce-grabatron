@extends('layouts.navbar')
@section('title')
    <title>About Us</title>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">About</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-edit"></i></button>
          </div>
        </div>
        {{-- @foreach ($about as $about) --}}

        <div class="card-body">
            <form action="{{route('about.update',['id'=>$about->id])}}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <textarea class="form-control" id="exampleFormControlTextarea1" name="deskripsi" rows="3">{{$about->deskripsi}}</textarea>
                        <button class="btn btn-sm btn-primary m-3" type="submit">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                </div>
            </form>
        </div>
        {{-- @endforeach --}}
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
