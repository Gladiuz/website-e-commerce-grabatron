<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kategori extends Model
{
    //
    protected $table = 'kategori_barang';
    protected $primaryKey = 'id_kategori';

    protected $fillable = [
        'nama_kategori','status'
    ];

    public function product()
    {
        return $this->hasMany('App\product');
    }
}
