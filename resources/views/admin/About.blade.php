@extends('layouts.navbar')
@section('title')
    <title>About Us</title>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      @foreach ($about as $about)
        <div class="card">
            <div class="card-header">
            <h3 class="card-title">About</h3>

            <div class="card-tools">
                <a href="{{route('about.edit',['id' => $about->id])}}">
                    <button type="button" class="btn btn-tool" title="Collapse">
                        <i class="fas fa-edit"></i></button>
                </a>
            </div>
            </div>


            <div class="card-body">
                {{$about->deskripsi}}
            </div>
        <!-- /.card-body -->
        </div>
        @endforeach
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


@endsection
