@extends('layouts.navbar')
@section('title')
    <title>Edit User {{$user->username}}</title>
@endsection

@section('content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Banner Create</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Banner</li>
              <li class="breadcrumb-item active">Banner Create</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Banner Create</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="{{route('Users.update',['id'=>$user->id])}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                  <div class="card-body">
                    <div class="form-group">
                        <label class="col-md-2 col-form-label {{$errors->has('name_user') ? 'is-invalid' : ''}}" for="text-input">Nama User :</label>
                        <div class="col-md-12">
                            <input class="form-control" id="text-input" type="text" name="name_user" value="{{$user->name_user}}" placeholder="Input name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-form-label {{$errors->has('email') ? 'is-invalid' : ''}}" for="text-input">Email :</label>
                        <div class="col-md-12">
                            <input class="form-control" id="text-input" type="text" name="email" value="{{$user->email}}" placeholder="Input Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-form-label" for="text-input">Username :</label>
                        <div class="col-md-12">
                            <input class="form-control {{$errors->has('username') ? 'is-invalid' : ''}}" id="text-input" type="text" name="username" value="{{$user->username}}" placeholder="Input Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-form-label" for="select1">Status</label>
                        <div class="col-md-12">
                        <select class="form-control" id="select1" name="level">
                            <option>Please select</option>
                            <option value="Admin" @if($user->level == 'Admin') selected @endif>Admin</option>
                            <option value="User" @if($user->level == 'User') selected @endif">User</option>
                        </select>
                        </div>
                    </div>
                </div>
            </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->
          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection

