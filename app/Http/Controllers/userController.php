<?php

namespace App\Http\Controllers;

use App\User;
use App\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class userController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    //
    public function index()
    {
        $users = DB::table('users')->get();
        $inboxes = DB::table('inbox')->paginate();

        $data = array(
            'users'=> $users,
            'inboxes'=> $inboxes,
        );

        return view('admin.User.user',$data);
    }

    public function edit($id)
    {
        $user = users::find($id);
        // $user->password = decrypt('password');
        $inboxes = DB::table('inbox')->paginate();

        $data = array(
            'user'=> $user,
            'inboxes'=> $inboxes,
        );

        return view('admin.User.user_edit',$data);
    }

    public function updateUser(Request $request, $id)
    {
        $user = users::find($id);
        $this->validate($request,[
            'name_user' => 'required',
            'username' => 'required|min:3',
            'email' => 'required|email',
        ]);

        $user->name_user = $request->name_user;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->level = $request->level;

        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $extension = $avatar->getClientOriginalExtension();
            $avatarname = time() . '.' . $extension;
            $avatar->move('css/img/' ,$avatarname);
            $user->avatar = $avatarname;
        }


        $user->save();

        return redirect('Users');

    }


}
