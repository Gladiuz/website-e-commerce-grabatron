<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('product', function(Blueprint $table) {
            $table->renameColumn('id','id_product');
        });

        Schema::table('users', function(Blueprint $table) {
            $table->renameColumn('id','id_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product', function(Blueprint $table) {
            $table->renameColumn('id_product','id');
        });

        Schema::table('users', function(Blueprint $table) {
            $table->renameColumn('id_user','id');
        });
    }
}
