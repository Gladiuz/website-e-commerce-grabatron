@extends('User.navtransaksi')
@section('title')
    <title>Check Out</title>
@endsection
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/bootstrap4/bootstrap.min.css')}}">
<link href="{{asset('user/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css')}}" rel="stylesheet" type="text/css">
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('user/plugins/OwlCarousel2-2.2.1/owl.carousel.css')}}"> --}}
{{-- <link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/owl.theme.default.css')}}"> --}}
{{-- <link rel="stylesheet" type="text/css" href="{{asset('user/plugins/OwlCarousel2-2.2.1/animate.css')}}"> --}}
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/cart_styles.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('user/styles/cart_responsive.css')}}">
{{-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> --}}
{{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous"> --}}
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
@endsection

@section('content')
<div class="cart_section">
    <div class="container wrapper">
        <div class="row">
            <div class="col-lg-7">
                <div class="cart_container">
                    <div class="panel-heading">Checkout Data</div>
                    <div class="panel-body">
                        <ul class="cart_list">
                            <li class="cart_item clearfix">
                                <form action="{{route('transaksi',['id' => $order->id_order])}}" method="get" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="inputEmail4">Nama Lengkap</label>
                                        <input type="text" class="form-control" id="inputEmail4" placeholder="Email" value="{{$user->name_user}}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="validationServerUsername">Username</label>
                                        <div class="input-group">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text" id="inputGroupPrepend3">@</span>
                                          </div>
                                          <input type="text" class="form-control" id="validationServerUsername" placeholder="Username" aria-describedby="inputGroupPrepend3" value="{{$user->username}}" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan Email" value="{{$user->email}}" disabled>
                                        <small id="emailHelp" class="form-text text-muted">Kami tidak akan membagikan email anda dengan siapapun.</small>
                                    </div>
                                    <div class="form-group">
                                      <label for="inputAddress">Alamat</label>
                                      <input type="text" class="form-control" name="alamat" id="inputAddress" placeholder="jl Mekar Mulya">
                                    </div>
                                    {{-- <div class="form-group">
                                      <label for="inputAddress2">Alamat 2</label>
                                      <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                                    </div> --}}
                                    <div class="form-group">
                                        <label for="inputZip">Kode Pos</label>
                                        <input type="text" class="form-control" id="inputZip" name="kode_pos" placeholder="00000">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPhone">Telephone</label>
                                        <input type="text" class="form-control" id="inputPhone" name="phone" placeholder="08132053441">
                                    </div>
                                    <input type="hidden" value="{{$harga_total['id_order']}}" name="id_order">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
                {{-- <div class="cart_container">
                        <div class="panel-heading">Ongkos Kirim</div>
                        <div class="panel-body">
                            <ul class="cart_list">
                                <li class="cart_item clearfix">
                                    <div class="">

                                    </div>
                                </li>
                            </ul>
                        </div>
                </div> --}}
            </div>
            <div class="col-lg-5">
                    <div class="cart_container">
                            <div class="panel-heading">Review Order</div>
                            <div class="panel-body">
                                <ul class="cart_list">
                                    <?php $total = 0 ?>
                                    @foreach ((array) session('cart') as $id => $details)
                                    <?php $total +=  + $details['harga'] * $details['quantity'] ?>
                                    <li class="cart_item clearfix">
                                        {{-- <div class="form-group"> --}}
                                            <div class="col-sm-4 col-xs-4 cart_item_image pt-2"><img src="{{asset('css/img/'. $details['gambar_product'])}}" alt="" class="img-fluid"></div>
                                            <div class="cart_item_text">{!! \Illuminate\Support\Str::words($details['nama_barang'], 3, '...')!!}</div>
                                            <div class="col-xs-12"><small>Quantity : <span>{{ $details['quantity'] }}</span></small></div>
                                                <div class="float-right">
                                                    <h6 class="">{{App\product::konversi($details['harga'] * $details['quantity'])}}</h6>
                                                </div>
                                        {{-- </div> --}}
                                    </li>
                                    @endforeach
                                    <div class="form-group"><hr /></div>
                                    <li class="cart_item clearfix">
                                            <div class="col-xs-12">
                                                    <strong>Ongkos Kirim : </strong>
                                                    <div class="float-right harga-ongkos"><h6 class="cart-total ongkirs">{{App\product::konversi($harga_total['ongkir'])}}</h6></div>                                                    <br>
                                                    <hr>
                                                    <strong>Total :</strong>
                                                    <div class="float-right"><h6 class="cart-total">{{App\product::konversi($harga_total['total_order'])}}</h6></div>
                                                    <input type="hidden" class="bpbp" value="{{$total}}">
                                                </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
            </div>

        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"crossorigin="anonymous"></script>
{{-- <script src="dist/jquery.simple-checkbox-table.min.js"></script> --}}
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script>
$(document).ready(function() {
    $('.subject-list').on('change', function() {
        $('.subject-list').not(this).prop('checked', false);
        var ele = $(this);
        // alert("test");

        var currow = $(this).closest("tr");
        var col5 = currow.find('td:eq(3)').text();
        var result = col5;
        var ongkir = $(".harga-ongkos").val(result);
        var order = $(".bpbp").val();

        var hasil = ongkir + order;


        if ($(".subject-list").length > 0)
        {
            // any one is checked
            // $(".help").text(result);
            ongkir.text(result);


            $('#hasil').text(ongkir);




        }
        else if($(".subject-list").length === 0)
        {
            // none is checked$
            $(".help").text("hai semua")
        }


    });
});
</script>
@endsection
